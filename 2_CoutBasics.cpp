/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 2_CoutBasics.cpp
 * @brief {演示在 C++ 中使用 cout 的基础知识.}
 ********************************************************************************/

#include <iostream>

using namespace std;

int main()
{
  // cout 是"标准输出流对象",<< 操作符是流插入操作符,
  // 此语句将输出文本 "test" 到标准输出流(默认情况下,控制台).
  cout << "test";
   
  // 将新的一行输出至控制台,以便后续输出打印下一行.
  cout << endl;
  
  // 我们必须输出一个 endl(或 \n)来获得新的一行,
  // 因为如果执行以下代码,abc123xyz 将在一行上输出所有的内容.
  cout << "abc";
  cout << "123";
  cout << "xyz";
  cout << endl;
  
  // 我们还可以使用特殊的换行符 \n 来输出新的一行.
  cout << "another line of text\n";
  
  // 我们可以在一个语句中,将多个流插入操作符链接在一起.
  cout << "let's learn how to " << "use cout!\n";
  
  // 我们可以在单个语句中插入多个 endl 流操作符.
  cout << "line 1" << endl << "line 2" << endl;
  
  // 我们可以使用插入操作符输出变量的值.
  int x = 20;
  cout << "x: " << x << endl;
  
  // 这对于输出动态的信息很有用,这些动态信息可能会随着我们程序
  // 作为输出的一部分运行而发生改变,
  // 例如:可能通过用户设置的年龄.
  int age = 45;
  cout << "His father is " << age << " years old.\n";
  
  return 0;
}