/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 27_Add_file_line_numbers.c
 * @brief {在 C 中给文件添加行号.我们通过创建一个临时文件来解决问题,
 *         将原始文件的行写入临时文件,前面有行号,
 *         然后删除原始文件并将临时文件重命名为原始文件的名称. 
 * 
 *         文件名本身作为命令行参数提供,
 *         所以我们将像这样运行程序：./add_file_line_numbers file.txt 其中 file.txt 是我们希望添加行号的文件的名称.}
 ********************************************************************************/

#include <stdio.h>
#include <string.h>

// 常量用于创建和使用 char 数组来存储临时文件名和原始文件的每一行.
#define FILENAME_SIZE 1024
#define MAX_LINE 2048

int main(int argc, char *argv[])
{
  // 访问原始文件和临时文件的文件指针.
  FILE *file, *temp;

  // 用于指向文件名命令行参数.
  char *filename;

  // 存储临时文件名.
  char temp_filename[FILENAME_SIZE];

  // 存储原始文件的每一行.
  char buffer[MAX_LINE];

  // 检查以确保提供了正确数量的命令行参数,如果没有,则退出并显示错误消息和状态.
  if (argc != 2)
  {
    printf("Argument missing.\n");
    return 1;
  }
  // 否则将文件名设置为指向第二个命令行参数字符串.
  else filename = argv[1];
  
  // 通过首先将字符串 "temp____" 复制到 temp_filename char 数组中来构建临时文件名,
  // 然后附加在原始文件名上,所以如果原始文件的名称是 file.txt 我们将有 "temp____file.txt".
  strcpy(temp_filename, "temp____");
  strcat(temp_filename, filename);
  
  // 打开原始文件进行读取,打开临时文件进行写入.
  file = fopen(filename, "r");
  temp = fopen(temp_filename, "w");
  
  // 如果其中一个或两个文件都无法打开,则退出并显示错误消息和状态.
  if (file == NULL || temp == NULL)
  {
    printf("Error opening file.\n");
    return 1;
  }
  
  // 跟踪当前行号以在临时文件的每一行上打印.
  int current_line = 1;
  
  // 将原始文件的每一行读入缓冲区,直到我们到达文件末尾.
  while (fgets(buffer, MAX_LINE, file) != NULL)
  {
    // 将 current_line 的 int 值写入临时文件,后跟一个空格,后跟原文件中的行.
    fprintf(temp, "%d %s", current_line, buffer);

    // 递增当前行,使其成为文件中下一行的正确值.
    current_line++;
  }
  
  // 关闭两个文件.
  fclose(temp);
  fclose(file);
  
  // 删除原文件,将临时文件重命名为原文件名.
  remove(filename);
  rename(temp_filename, filename);

  return 0;
}