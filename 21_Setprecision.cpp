/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 21_Setprecision.cpp
 * @brief {在 C++ 中使用 setprecision 流操纵器格式化浮点值输出的示例.}
 ********************************************************************************/

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
  double n = 42.123456;
  
  // setprecision 会将双精度浮点值(double)数 n 的输出位数限制为提供的参数 (5,4,3,...).
  cout << "5: " << setprecision(5) << n << endl;
  cout << "4: " << setprecision(4) << n << endl;
  cout << "3: " << setprecision(3) << n << endl;
  cout << "2: " << setprecision(2) << n << endl;
  cout << "1: " << setprecision(1) << n << endl;

  cout << endl;
  
  // 与 fixed 流操纵器结合使用时,
  // 我们可以让 setprecision 输出一定数量的 *decimal|十进制* 数字(小数点后的数字).
  // fixed 的流操纵器用于定点表示法.
  cout << "5: " << fixed << setprecision(5) << n << endl;
  cout << "4: " << fixed << setprecision(4) << n << endl;
  cout << "3: " << fixed << setprecision(3) << n << endl;
  cout << "2: " << fixed << setprecision(2) << n << endl;
  cout << "1: " << fixed << setprecision(1) << n << endl;
  
  cout << endl;
  
  // 我们可以使用 scientific 来获得数字 n 的科学计数法输出,
  // 再次带有一定数量的十进制数字.
  cout << "5: " << scientific << setprecision(5) << n << endl;
  cout << "4: " << scientific << setprecision(4) << n << endl;
  cout << "3: " << scientific << setprecision(3) << n << endl;
  cout << "2: " << scientific << setprecision(2) << n << endl;
  cout << "1: " << scientific << setprecision(1) << n << endl;
  
  cout << endl;
  
  double n1 = 34.2;
  double n2 = 23.456;
  
  // 一起使用 fixed 和 setprecision 时,即使这些数字是 0,我们也会得到输出的小数位数.
  // 因此,例如在 n1 的情况下,我们将得到 34.200,即使最后 2 位数字并没有真正提供更多信息,
  // 那是因为 setprecision(3) 将确保数字以 3 个十进制数字输出.
  cout << "n1: " << fixed << setprecision(3) << n1 << endl;
  cout << "n2: " << fixed << setprecision(3) << n2 << endl;

  cout << endl;
  
  return 0;
}