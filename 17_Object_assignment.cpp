/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 17_Object_assignment.cpp
 * @brief {演示对象赋值如何在C++中工作, 
 *         包括一个例子:如果我们的对象包含指向堆(heap)上数据的指针的成员变量,为什么我们需要小心对象分配.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 具有单个公共(public)int类型的成员变量的简单对象.
class Simple
{
public:
  int x;
};

// 具有本身就是对象的成员变量的对象.
class Stack
{
public:
  Simple simple;
};

// 具有成员变量的对象,该成员变量是指向堆(heap)上数据的指针.
class Heap
{
public:
  Simple *simple;
  
  Heap(int set_x)
  {
    simple = new Simple;
    simple->x = set_x;
  }
  
};

int main()
{
  // 在主函数下方的注释中查看内存的可视化. 
  // 对于这 3 个例子...

  // 示例 1:
  // 将一个对象分配给另一个对象.

  // 创建 2 个 Simple 对象,并为成员变量 x 赋值.
  Simple simpleA;
  simpleA.x = 4;
  Simple simpleB;
  simpleB.x = 0;

  // 当我们将 SimpleA 分配给 SimpleB, 
  // SimpleA 的成员变量 x 的值将被赋值给 SimpleB 的成员变量 x 的值,
  // 我们称之为浅拷贝("shallow copy").
  simpleB = simpleA;

  // 我们会发现 2 个 x 值是相同的(output:4).
  cout << "simpleA.x: " << simpleA.x << endl;
  cout << "simpleB.x: " << simpleB.x << endl;

  // 如果我们改变 SimpleB 的成员变量 x,这不会影响 SimpleA 的
  // 成员变量 x,因为它们是内存中的 2 个不同的对象.(output:4;20)
  simpleB.x = 20;
  cout << "simpleA.x: " << simpleA.x << endl;
  cout << "simpleB.x: " << simpleB.x << endl;


  // 示例 2:
  // 当对象的成员变量本身就是对象时,将对象分配给另一个对象.

  // 创建 2 个 Stack 对象,它们都将 Simple 对象作为成员变量...
  // 分配给这些 Simple 对象的 x 成员变量.
  Stack stackA;
  stackA.simple.x = 4;
  Stack stackB;
  stackB.simple.x = 0;

  // 当我们将 stackA 赋值给 stackB 时,Simple 对象成员变量会发生赋值操作,
  // 然后像示例 1 中一样,分配成员变量 x 值.(output:4;4)
  stackB = stackA;
  cout << "stackA.simple.x: " << stackA.simple.x << endl;
  cout << "stackB.simple.x: " << stackB.simple.x << endl;

  // 再次,如果我们修改 stackB 的 Simple 对象的 x 成员变量,
  // 我们会发现这不会修改 stackB 的 Simple 对象的 x 成员变量...
  // 因为这些都是内存中不同的对象...改变一个不会影响另一个.(output:4;20)
  stackB.simple.x = 20;
  cout << "stackA.simple.x: " << stackA.simple.x << endl;
  cout << "stackB.simple.x: " << stackB.simple.x << endl;


  // 示例 3:
  // 当对象成员变量包含指向堆(heap)上数据的指针时，将一个对象分配给另一个对象.
  
  // 创建 2 个堆(heap)对象,堆(heap)对象的构造函数将创建一个 Simple 对象
  // 并分配给 Simple 对象指针成员变量"简化"了该对象在堆上的内存地址.
  Heap heapA(4);
  Heap heapB(0);

  // 当我们将 heapA 分配给 heapB 时,就像上面两个例子一样, 
  // heapA 的成员变量的值将被赋值给 heapB 的成员变量.
  // 但是在这种情况下,成员变量 simple 是一个 POINTER，它存储了堆(HEAP)上 Simple 对象的内存地址.
  // 所以 heapB 的简单指针成员变量在赋值后会和 heapA 的简单指针成员变量一样存储相同的内存地址...
  // 换句话说,在这个赋值之后,它们将指向堆上的同一个对象!
  heapB = heapA;

  // 两个 x 值都将输出为“4”,这可能会给我们一种印象,即我们得到了与示例 1 和示例 2 相同的行为,
  // 但这次真正发生的是 heapA 和 heapB 的 simple 指针成员变量都指向同 1 个 simple 对象.
  cout << "heapA.simple->x: " << heapA.simple->x << endl;
  cout << "heapB.simple->x: " << heapB.simple->x << endl;
  //output:4;4

  // 当我们改变 heapB 的 simple 指针成员变量指向的对象的 x 值时,
  // 我们修改了 heapA 和 heapB 现在都指向的对象!
  // 因此，我们将得到与 "两个" x 值的输出相同的结果(20)(实际上它们是同一个对象).
  heapB.simple->x = 20;
  cout << "heapA.simple->x: " << heapA.simple->x << endl;
  cout << "heapB.simple->x: " << heapB.simple->x << endl;
  //ouput:20;20

  return 0;
}

//
//
//   示例 1 内存视图:
//
//                                               STACK|栈
//                                               =====
//
//    ------------       ------------
//   | simpleA    |     | simpleB    |
//   |            |     |            |
//   | int x = 4  |     | int x = 20 |
//    ------------       ------------
//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//                                               HEAP|堆
//                                               ====
//
//                Nothing
//
//
//
//
//
//   示例 2 内存视图:
//
//                                               STACK|栈
//                                               =====
//
//    ---------------       ---------------
//   | stackA        |     | stackB        |
//   |               |     |               |
//   | Simple simple |     | Simple simple |
//    --------- | ---       --------- | ---
//              |                     |
//              |                     |
//          ------------       ------------
//         | simple     |     | simple     |
//         |            |     |            |
//         | int x = 4  |     | int x = 20 |
//          ------------       ------------
//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//                                               HEAP|堆
//                                               =====
//
//                     Nothing
//
//
//
//
//
//  示例 3 内存视图:
//
//                                               STACK|栈
//                                               =====
//
//    ----------------       ----------------
//   | heapA          |     | heapB          |
//   |                |     |                |
//   | Simple *simple |     | Simple *simple |
//    --------- | ----       --------- | ----
//              |                     /
//              |           ----------
//              |          /
// ~~~~~~~~~~~~ | ~~~~~~~ / ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//              |        /
//              |       /                        HEAP|堆
//              |      /                         =====
//         ------------        ------------
//        | simple     |      | simple     |
//        |            |      |            |
//        | int x = 20 |      | int x = 0  |
//        ------------         ------------
//