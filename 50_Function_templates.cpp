/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-27
 * 
 * @file 50_Function_templates.cpp
 * @brief {在 C++ 中使用函数模板减少代码重复的示例.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 这个 template(模板)前缀允许我们创建一个函数模板 find_max,带有单个类型参数 T,我们也可以将其称为模板变量.
// 当我们在程序中调用 find_max 时,编译器将在编译时创建并使用 find_max 函数的类型特定版本,称之为模板函数
// (因此,如果我们用 ints 调用 find_max,则 T 将被替换为 int,如果我们用 double 调用 find_max,则 T 将被替换为 double).
// find_max 函数将在 a 与 b 之间找到它们的最大值.
template <typename T>
T find_max(T a, T b)
{
  if (a > b) return a;
  else return b;
}

// 我们可以通过逗号(,)分隔来创建具有多个类型参数的函数模板.
// 在这里,我们使用两个不同的类型参数 T1 和 T2 返回两个参数 a 和 b 的最大 size(以字节为单位).
template <typename T1, typename T2>
int find_max_size(T1 a, T2 b)
{
  if (sizeof(a) > sizeof(b)) return sizeof(a);
  else return sizeof(b);
}

int main()
{
  // 创建 2 个 doubles,并寻找最大值.
  double x = 10.6;
  double y = 5.3;
  
  // 使用我们的函数模板 find_max 并显式提供 'double' 作为模板形参(parameter) T 的实参(argument).
  // 编译器将创建一个特定于 double 类型的 find_max 函数版本,并在此处使用它.
  double max_double = find_max<double>(x,y);
  
  // 验证是否返回了正确的最大 double 值. 
  cout << "max_double: " << max_double << endl;
  

  // 创建 2 个 ints,并寻找最大值.
  int m = 5;
  int n = 3;
  
  // 再次使用我们的函数模板 find_max,这次使用 int 实参(arguments).
  // 请注意:如果我们没有显式提供像 fi​​nd_max<int>(m,n) 这样的类型,它仍然可以工作,因为编译器可以根据我们的实参(arguments)找出所需的类型
  int max_int = find_max(m,n);
  
  // 验证是否返回了正确的最大 int 值.
  cout << "max_int: " << max_int << endl;
  

  // 使用 x、m、c、b 等不同的实参(arguments)在下面尝试 max_size.
  char c;
  int16_t b;

  // 查找提供的两个参数(arguments)的最大 size...
  // 可以使用任何类型的组合，因为我们在我们的函数模板 find_max_size 中有两个类型参数(parameters).
  int max_size = find_max_size(c, b);
  
  // 验证输出两个参数(arguments)的正确最大 size.
  cout << "max_size: " << max_size << endl;
  
  return 0;
}


// 在编译时,基于我们在函数调用中如何使用函数模板,
// 编译器将决定生成要在运行时使用的函数的不同类型特定版本(称为模板函数).
// 该过程可视化如下:
//
//
// double x = 10.6                           int m = 5;
// double y = 5.3;                           int n = 3;
//
// find_max(x,y)       <- 函数调用 ->        find_max(m,n)
//              \                           /
//               \                         /
//                \                       /
//                 \                     /
//
//                  template <typename T>
//                  T find_max(T a, T b)
//  函数            {
//  模板   ->       if (a > b) return a;
//                    else return b;
//                  }
//
//                 /               \
//                /                 \
//               /                   \
//              /                     \
//
// double find_max(double a,       int find_max(int a,
//                 double b)                    int b)
// {                               {
//   if (a > b) return a;            if (a > b) return a;
//   else return b;                  else return b;
// }                               }
//
//                \                  /
//                 Template Functions(模板函数)