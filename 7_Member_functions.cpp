/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 7_Member_functions.cpp
 * @brief {演示在C++中使用类成员函数(也称方法)的基础知识.}
 ********************************************************************************/

#include <iostream>

using namespace std;

//定义一个矩形(Rectangle)类
class Rectangle
{

// 私有(private)成员只能在类内访问,不能再类外访问.
private:

  // 私有成员变量长度(length)、宽度(width)(分别对应矩形的长度、宽度)
  double length;
  double width;
  
  // 成员函数也可以是私有(private)的,我们不能在矩形(Rectangle)类之外的区域进行访问.
  // 成员函数也可以返回一个值,如下 area() 函数将返回一个双精度的值.
  double area()
  {
    return length * width;
  }
  
// 公共(public)访问说明符将使得这些成员函数在类之外被可用(例如:在下面的主函数中).
public:
  
  // 成员函数可以接受参数,也可以包含参数的默认值.
  // (如果未提供,此处 w 设置为默认值 5).
  void set_dimensions(double l, double w = 5)
  {
    // 成员函数可以访问和使用成员变量,包括私有成员变量.
    length = l;
    width = w;
  }
  
  // 只要我们在类中提供声明(又称原型),就可以在类定义之外定义成员函数.
  double perimeter();
  
  // 成员函数可以访问其他成员函数,包括 area() 等私有成员函数.
  void print_area()
  {
    cout << "Area: " << area() << endl;
  }
  
};

// 我们可以使用类名(Rectangle):: 将下面的函数定义连接到上面定义的类.
double Rectangle::perimeter()
{
  return 2 * (length + width);
}

int main()
{
  // 创建一个矩形(Rectangle)对象实例.
  Rectangle rectangle1;
  
  // 使用 set_dimensions() 函数来设置长度(length)与宽度(width)成员.
  rectangle1.set_dimensions(10, 20);

  // 使用其他公共(public)成员函数输出周长(perimeter())、面积(area())
  cout << "Perimeter: " << rectangle1.perimeter() << endl;
  rectangle1.print_area();
  
  // 创建另外一个矩形(Rectangle)对象实例.
  Rectangle rectangle2;
  
  // 如果我们只为 set_dimensions 函数提供一个参数,则使用参数 w(宽度)的默认值.
  rectangle2.set_dimensions(5);
  
  // 输出第二个矩形(Rectangle)实例的周长和面积.
  cout << "Perimeter: " << rectangle2.perimeter() << endl;
  rectangle2.print_area();
  
  return 0;
}