/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 15_Constructor_deltgation.cpp
 * @brief {示例:在C++中使用构造函数委托,让一个构造函数调用同一个
 *         类中的另外一个构造函数,这有助于减少代码的重复.}
 ********************************************************************************/

#include <iostream>

using namespace std;

class Rectangle
{
public:
  int length;
  int width;
  int area;
  string color;
  
  // 此构造函数初始化长度(length)与宽度(width)成员变量, 
  // 并且计算和初始化面积(area)成员变量,但不初始化颜色(color) 
  // 然后将其保留为空字符串(可能没问题,也许是可选的).
  Rectangle(int l, int w)
  {
    length = l;
    width = w;
    area = length * width;
    cout << "Constructor 1" << endl;
  }
  
  // 这个构造函数使用构造函数委托来调用上面的构造函数,并使用它提供的 l 和 w 值,
  // 它依靠该构造函数来初始化长度(length)、宽度(width)和面积(area)成员变量.
  // 在这个构造函数调用完成后,这个构造函数的代码会被执行,它会初始化颜色(color)成员变量.  
  // 这减少了代码重复,否则这个构造函数也需要初始化这些成员变量.并且它会重复完全相同的代码来这样做!
  Rectangle(int l, int w, string c) : Rectangle(l,w)
  {
    color = c;
    cout << "Constructor 2" << endl;
  }
  
  // 取出 Rectangle 对象的相关信息.
  void print()
  {
    cout << "length: " << length << endl;
    cout << "width: " << width << endl;
    cout << "area: " << area << endl;
    cout << "color: " << color << endl;
  }
  
};

int main()
{
  // 当我们使用带有 3 个参数的构造函数来创建一个 Rectangle 对象时,
  // 注意:"Constructor 1"如何首先输出,然后是"Constructor 2",这是因为构造函数委托!
  // 接受 3 个参数的构造函数第 1 次调用只接受 2 个参数的构造函数,然后执行自己的代码.  
  Rectangle rectangle1(5, 10, "red");
  
  rectangle1.print();
  
  return 0;
}