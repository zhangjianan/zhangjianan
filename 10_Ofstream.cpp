/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 10_Ofstream.cpp
 * @brief {演示如何使用ofstream在C++中写入文件.}
 ********************************************************************************/

#include <iostream>
#include <fstream>

using namespace std;

int main()
{
  // 创建一个 ofstream 对象
  ofstream outFile;

  // 使用 append 标志打开 file.txt,因此内容将被附加(appended)到 file.txt 中任何现有内容的末尾.
  outFile.open("./file/file.txt", ofstream::app);

  // 检查文件是否无法打开,若打开失败则报错.
  if (outFile.fail())
  {
    cout << "Error opening file." << endl;
    return 1;
  }

  // 我们使用流插入操作符来写入文件,就像使用 cout 一样.
  outFile << "content" << endl;

  // 声明和初始化一些不同类型的变量.
  double x = 4.5;
  int y = 10;
  string z = "abc";

  // 我们也可以输出变量的内容!
  outFile << x << endl;
  outFile << y << endl;
  outFile << z ;

  // 关闭文件,作为完成后的最佳操作.
  outFile.close();

  return 0;
}