/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 19_Function_overloading.cpp
 * @brief {在 C++ 中使用函数重载在同一作用域内拥有多个同名函数的示例.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 2 个 int 参数相加的函数-add()
int add(int x, int y)
{
  return x + y;
}

// 3 个 int 参数相加的函数-add()... 
// 因为参数的数量与上面的函数不同,C++ 将允许这样做.
int add(int x, int y, int z)
{
  return x + y + z;
}

// 3 个 double 参数相加的函数-add()...
// 因为参数的类型与上面的函数不同,C++ 也会允许这样做!
double add(double x, double y, double z)
{
  return x + y + z;
}

// 即使参数的名称不同,
// 我们会在下面的 add 函数中得到一个错误,因为参数的数量和类型与上面的 add 函数相同!
/*
double add(double a, double b, double c)
{
  return a + b + c;
}
*/

// 成员函数也可以重载,因为这个 Add 类有两个名为 add 的成员函数.
class Add
{
public:
  
  // 具有两个 int 参数相加的 add 函数
  int add(int x, int y)
  {
    return x + y;
  }
  
  // 具有两个 double 参数相加的 add 函数,
  // 因为参数的类型不同(在这种情况下,两个参数的类型),C++ 将允许这样做.
  double add(double x, double y)
  {
    return x + y;
  }
  
};

int main()
{
  // 调用 3 个不同的相加函数-add().
  cout << add(10,5) << endl;
  cout << add(10,5,2) << endl;
  cout << add(10.5,3.2,4.1) << endl;

  // 如果我们尝试以下操作,将导致错误!因为我们打算调用哪个 add 函数不明确.
  // 在上面的浮点数 10.5 中,很明显我们打算用 3 个 double 类型的参数调用 add 函数, 
  // 但整数 10 使其明确.
  // cout << add(10,3.2,4.1) << endl;

  
  // 创建一个对象.
  Add add_obj;
  
  // 测试"int"和"double" add 的成员函数
  cout << add_obj.add(10,5) << endl;
  cout << add_obj.add(10.5,4.5) << endl;
  
  return 0;
}