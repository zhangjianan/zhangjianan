/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 38_Clear_input_buffer.cpp
 * @brief {在 C++ 中使用 cin.ignore() 清除输入缓冲区的示例.}
 ********************************************************************************/

#include <iostream>

using namespace std;

int main()
{
  // 尝试读取 int 并将其存储到 number.
  // 当我们使用 cin 时,用户输入的字符最终会出现在“输入缓冲区”中,
  // 值存储到变量(如 number)之前的临时位置.
  int number = 0;
  cout << "number: ";
  cin >> number;
  
  // 在用户输入像“5”这样的数字并点击输入后,输入缓冲区将包含字符:
  //
  // 5\n
  //
  // 然后 cin >> number 语句将撕掉字符 5 以将 int 5 存储到 number 变量中.
  // 这将使输入缓冲区留下字符:
  //
  // \n
  //
  // 问题是下面尝试读取用户输入的一行将失败,因为 getline() 在遇到的第一个换行符处停止读取输入,
  // 在这种情况下,我们*已经*在输入缓冲区中有一个换行符,所以 getline() 立即停止而不是暂停用户输入.
  //
  // 我们可以使用 cin.ignore(#, xchar) 忽略输入缓冲区中最多 # 个字符(“撕掉它们”)或直到第一个 xchar (其中 xchar 是某个字符),以先到者为准.
  // 所以如果我们要跑:
  //
  // cin.ignore(1, '\n');
  //
  // 这将从输入缓冲区中删除换行符,
  // 并且 getline() 将允许用户输入一个字符串,因为缓冲区将是空的.
  // 但是如果用户输入这样的内容:
  //
  // 5----\n
  //
  // 其中每个 - 代表一个空格字符,那么我们需要忽略最多 5 个字符才能使输入流再次为空白!
  // 所以我们使用:
  // 
  // cin.ignore(numeric_limits<streamsize>::max(), '\n');
  //
  // 其中 numeric_limits<streamsize>::max() 是输入缓冲区中可能出现的最大字符数,
  // 确保在运行 cin.ignore() 后必须完全清除缓冲区.
  //
  // 如果我们注释掉下面的行,请注意对 getline() 的调用是如何失败的!
  //
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  // 读取用户输入的一行字符串,getline 将在第一个换行符处停止读取字符(最多 256 个字符).
  char line[256];
  cout << "line: ";
  cin.getline(line, 256);
  
  // 输出数字和行值.
  cout << endl;
  cout << "number: " << number << endl;
  cout << "line: " << line << endl;
  
  return 0;
}