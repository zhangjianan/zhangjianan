/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 20_Setw.cpp
 * @brief {演示如何在 C++ 中使用 setw 流操纵器将数据输出到具有指定字符宽度的字段中.}
 ********************************************************************************/

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
  // 使用 setw 的基本示例.
  // setw(5) 将导致流中的下一个值输出到宽度为 5 个字符的字段中.
  // 我们可以使用 left/right 来修改该字段的对齐方式,并使用 setfill 设置与默认空格字符不同的填充字符.
  //
  // 取消注释下面的代码以尝试一下(尽管将填充字符设置为 * 将使下表输出带有星号而不是空格). 
  //
  /*
  int area = 123;
  cout << left << setw(5) << setfill('*');
  cout << area << endl;
  cout << endl << 12345 << endl;
  */
  
  // setw 的一个实际用例是输出一个数据表,使得每一列都有一组一致的宽度.
  // 让我们看看下面的例子!

  // 输出表格标题.
  cout << endl;
  cout << "Rectangle Area Table";
  cout << endl << endl;
  
  // 创建一个包含 3 列的表格: length, width and area.
  // 该表将显示具有相关长度(lengths)和宽度(widths)的矩形的矩形区域.  
  // 我们将使长度和宽度列宽度为 10 个字符并左对齐,区域列宽度为 4 个字符并右对齐.
  // 我们将使用 setw 和 left/right 创建此格式,如下所示,
  // 然后我们将以完全相同的方式使用这些流操作符为表的字段输出 DATA.
  //
  cout << left;
  cout << setw(10) << "Length";
  cout << setw(10) << "Width";
  cout << right;
  cout << setw(4) << "Area";
  cout << endl;
  
  // 由于我们的表格宽度为 24 个字符(10+10+4),因此输出 24 个 * 字符以将列标题与数据分开.
  for (int i = 0; i < 24; i++) cout << "*";
  cout << endl;
  
  // 长度范围从 5 到 25 x 5.
  for (int length = 5; length <= 25; length += 5)
  {
    // 宽度范围从 2 到 10 x 2
    for (int width = 2; width <= 10; width += 2)
    {
      // 将长度和宽度输出到左对齐的 10 个字符宽度字段中.
      cout << left;
      cout << setw(10) << length;
      cout << setw(10) << width;

      // 将区域输出到右对齐的 4 字符宽度字段中.
      cout << right;
      cout << setw(4) << length * width;

      // 输出结束行,因此下一行输出从下一行开始.
      cout << endl;
    }
  }
  
  // 用一些新的/空白行结束程序.
  cout << endl << endl;
  
  return 0;
}