/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 9_Destructor_basics.cpp
 * @brief {演示在C++中使用析构函数的基础知识.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 一个简单的 "Number" 类
class Number
{
  
// 该类有一个私有成员变量:一个指向双精度的指针.
private:
  double *number;
  
public:
  
  // 构造函数
  Number(double num)
  {
    // 为双精度值分配内存空间,让私有成员变量 number 指向该空间. 
    number = (double *) malloc(sizeof(double));

    // 对指针解引用(dereference)(获取指针地址或引用地址上的值),将值 num 存储在分配的内存空间中.
    *number = num;

    // 输出语句:用于跟踪调用了构造函数时执行的情况.
    cout << "Constructor executing!" << endl;
    cout << "Number: " << *number << endl << endl;
  }
  
  // 析构函数在对象被销毁时运行, 
  // 并且通常用于清理工作,例如释放动态分配的内存.
  // 我们在类名的前面加上 ~ 符号来定义析构函数,并且析构函数不能带有参数.
  ~Number()
  {
    // 输出语句:用于跟踪调用了析构函数时执行的情况.
    cout << "Destructor executing!" << endl;
    cout << "Number: " << *number << endl << endl;

    // 释放动态分配的内存,以使得再次可用,防止内存泄露.
    free(number);
  }
  
};

// 在本地创建一个 Number 对象的基本函数.
void test()
{
  // Number 对象在此处被创建,并在函数完成时销毁...
  // 销毁时,调用析构函数,但它是隐含的,因为我们没有编写任何语句来显式的调用它.
  Number six(6);
}

int main()
{
  // 当我们使用 'new' 在堆(heap)上创建一个动态分配内存的对象时,将调用构造函数.
  Number *five = new Number(5);

  // 当我们使用 delete 来销毁对象并释放内存时, 
  // 析构函数则会被调用,从这个意义上说,我们更明确地 "calling" 析构函数. 
  delete five;

  // 调用 test() 函数时,将导致函数完成执行时创建和销毁对象.
  test();
  
  // 我们创建一个在程序终止时将被销毁的对象,此时调用析构函数.
  Number seven(7);

  return 0;
}