/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 4_StringBasics.cpp
 * @brief {在C++中使用字符串类型的基础知识.}
 ********************************************************************************/

#include <iostream>

// 即使某些 C++ 实现会自动为我们包含字符串(string)库,
// 我们仍然应该包含它,以使得我们的程序更具有可移植性.
#include <string>

using namespace std;

int main()
{
  // 创建一个名为 test1 的字符串变量,为其分配一个字符串文字.
  string test1 = "abcdefghi";
  
  // 我们可以使用 cout 和 << 输出字符串.
  cout << test1 << endl;
  
  // 输出字符串的大小(i.e. 即.字符数量).
  cout << "size: " << test1.size() << endl;
  
  // 我们可以使用 [] 和 .at() 来访问字符串中每个索引处的单个字符. 
  cout << "test1[1]: " << test1[1] << endl;
  cout << "test1.at(3): " << test1.at(3) << endl;
  
  // 我们也可以使用 [] 语法来修改字符串中的单个字符.
  test1[2] = 'C';
  cout << test1 << endl;
  
  // 有几种方法可以连接、附加字符串.
  test1 = test1 + "jklmnop";
  test1 += "qrstu";
  test1.append("vwxyz");
  cout << test1 << endl;
  
  // length() 函数是 size() 函数的同义词,它返回相同的值.
  cout << "length: " << test1.length() << endl;
  
  // 我们可以创建更多的字符串类型的变量.
  string test2 = "123";
  string test3;
  
  // 我们可以连接字符串变量,而不仅仅是字符串文字.
  test3 = test1 + test2;
  cout << "test3: " << test3 << endl;
  
  // empty() 将检查字符串是否为空 (显然 test3 则不是)
  if (test3.empty()) cout << "test3 is empty" << endl;
  else cout << "test3 is not empty" << endl;
  
  // clear() 将字符串设为空.
  test3.clear();
  cout << "test3: " << test3 << endl;
  
  // test3 现在应该是空的!
  if (test3.empty()) cout << "test3 is empty" << endl;
  else cout << "test3 is not empty" << endl;
  
  // 我们可以使用 to_string() 将 ints 和 doubles 等值转换为字符串.
  string test4 = to_string(-10.5);
  cout << test4 << endl;
  
  // 可以使用 stod() 将 double 值转换为字符串,
  // 其它函数也可以转换为其它类型.
  double number = stod(test4);
  cout << "number: " << number << endl;
  
  // 存在许多用于处理字符串的函数,例如,可以使用
  // substr() 从字符串中提取子字符串,在这种情况下, 
  // 从索引 2 处的字符开始提取 4 个字符 (i.e. 即 "test")
  string test5 = "A test string";
  string sub = test5.substr(2,4);
  cout << "sub: " << sub << endl;
  
  // 可以将输入存储至字符串类型的变量中,例如:name
  string name;
  cout << "Enter name: ";

  // 将字符串存储到第一个空格字符为止,因此像 
  // 'kevin browne' 这样的名字将仅作为 'kevin' 存储到 name,  
  //cin >> name;

  // 将字符串存储到 name 中的第一个换行符为止,
  // 所以像 'kevin browne' 这样的名字将被完全存储.
  getline(cin, name);

  // 输出 name
  cout << "Name: " << name << endl;

  return 0;
}