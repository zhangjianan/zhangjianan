/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 18_Copy_constructor.cpp
 * @brief {在 C++ 中创建和使用复制构造函数的示例.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 一个没有动态分配内存的简单对象.
class BasicNumber
{
public:
  
  // BasicNumber 对象将存储一个整数.
  int n;
  
  // 我们的构造函数将初始化对象的整数成员变量.
  BasicNumber(int set_n)
  {
    n = set_n;
  }
  
  // 复制构造函数是一个特殊的构造函数,它使用另一个相同类型的对象来初始化对象.
  // 我们通过加倍(doubling)另一个对象的 n 成员变量来设置成员变量 n.
  // 如果我们不创建自己的自定义复制构造函数,C++ 将自动创建一个默认的
  // 复制构造函数,它将一个对象的成员变量设置为另一个对象的成员变量的值(成员分配).
  // 我们称之为对象的“浅拷贝”,可能会成为一个问题,即我们的对象使用动态分配的数据 (
  // BasicNumber 不使用动态分配的数据,这个复制构造函数是为了说明这个概念).
  BasicNumber(const BasicNumber& basic_number)
  {
    n = 2 * basic_number.n;
    cout << "Copy constructor called: " << n << endl;
  }
};

// Number对象有一个成员变量,指向动态分配数据,
// 我们提供了一个复制构造函数来执行“深拷贝”而不是“浅拷贝”,这样我们就不会遇到问题!
class Number
{
public:

  // 成员变量是指向 int 值的 *pointer*,它将存储 int 值的内存地址.
  int *n;
  
  // 我们自己的构造函数.
  Number(int set_n)
  {
    // 在堆(heap)上为一个 int 动态分配空间，让 n 存储这个 int 的内存地址上.
    n = (int *) malloc(sizeof(int));

    // 解引用指针 n 以设置 n 存储到 set_n 的内存地址处的值
    *n = set_n;
  }
  
  // 复制构造函数通过使用另一个 Number 对象对对象进行深拷贝.
  Number(const Number& otherNumber)
  {
    // 和上面的构造函数类似,为一个 int 动态分配空间.
    // 这是制作对象“深拷贝”的关键...
    // 我们将在堆上给这个对象它自己的空间,然后它的成员变量 n 将指向.
    n = (int *) malloc(sizeof(int));

    // 我们将 n 指向的堆上的 int 赋值给另一个对象的指针成员变量 n 指向的值,完成深拷贝. 
    // 我们已经为该值分配了空间,现在我们将另一个对象具有的相同值存储到该空间中.
    *n = *(otherNumber.n);
  }
  
  // 当 Number 对象被删除时,我们将释放动态分配的内存.
  ~Number()
  {
    free(n);
  }
  
  // 返回 n 指向的 int 值.
  int get()
  {
    return *n;
  }
};

int main()
{
  // 示例 1 - 复制构造函数

  // 使用我们的构造函数(而不是复制构造函数)创建BasicNumber对象.
  BasicNumber num1(7);
  
  // 输出 num1 存储的 int.
  cout << "num1: " << num1.n << endl;
  
  // 使用我们创建的复制构造函数创建对象 num2.
  BasicNumber num2 = num1;
  
  // 当我们输出 num2 存储的 int 时,我们会发现它是 num1 存储的 int 的两倍
  // 因为我们的复制构造函数在分配它之前将值加倍!
  cout << "num2: " << num2.n << endl;
  
  // 使用我们的构造函数(不是复制构造函数)再创建两个 BasicNumber 对象.
  BasicNumber num3(5);
  BasicNumber num4(10);
  
  // 下面的这条语句不应与对象初始化相混淆,对象初始化是复制构造函数将运行的地方之一. 
  // 下面的这条语句是赋值运算符,它是一种不同类型的操作不运行我们的复制构造函数.
  // 我们创建的复制构造函数也可能在其他一些实例中运行,例如:当函数将对象作为值返回时, 
  // 或者当参数按值传递时,函数接收对象作为参数时.
  num3 = num4;
  
  
  // 示例 2 - 复制构造函数执行深拷贝. 

  // 使用构造函数(不是复制构造函数)创建一个数字对象.
  Number numA(8);

  // 输出 numA 的指针成员变量指向的值.
  cout << "numA: " << numA.get() << endl;
  
  // 使用复制构造函数创建 numB.
  Number numB = numA;
  
  // 输出 numB 的指针成员变量指向的值.
  cout << "numB: " << numB.get() << endl;

  // 如果我们修改 numA 的指针成员变量指向的值, 
  // 我们会发现它对 numB 没有影响 *因为*我们已经实现了深拷贝.
  *(numA.n) = 20;
 
  // 输出 numA 和 numB 的指针成员变量所指向的值,我们会发现它们不一样!
  cout << "numA: " << numA.get() << endl;
  cout << "numB: " << numB.get() << endl;

  // JUST FOR FUN...  :-)
  //
  // 尝试删除我们在 Number 类中定义的复制构造函数！如果你这样做,C++ 提供的默认复制构造函数将被使用在上面的代码中.
  // 默认的复制构造函数对对象进行浅拷贝... 将 numA.n 的指针值(内存地址)分配给 numB.n.
  // 问题是 numA.n 和 numB.b 都将存储相同的内存地址... 即他们在内存中引用相同的 int 值.
  // 这将产生多种后果... 在上面的代码中我们将 numA.n 指向的值更改为 20, 它将导致
  // numB.n 也指向 20,因为 numA.n 和 numB.n 正在存储相同的内存地址!
  // 当 numA 和 numB 的析构函数都运行时,程序也会崩溃.
  // 当 numA 的析构函数运行时,它将释放动态分配的内存...
  // 但是当 numB 的析构函数运行并尝试释放已释放的相同内存时将导致我们的程序崩溃!

  return 0;
}