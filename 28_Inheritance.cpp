/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 28_Inheritance.cpp
 * @brief {如何在 C++ 中使用继承的基本演示.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// MenuItem 是一个"基类"或者我们也可以称之为"父类",
// 它包含一些简单的成员变量和一个用于打印它们的成员函数.
class MenuItem//菜单
{
public:
  string name;
  double calories;
  
  void print()
  {
    cout << name << " (" << calories << " cal)" << endl;
  }
};

// 我们希望我们的 Drink 类"拥有并做" MenuItem 类可以做的所有事情...
// 我们可以将 MenuItem 中的代码复制并粘贴到 Drink 类中,
// 但是我们会有重复的代码(我们可以称之为"代码克隆").
// 重复代码的问题在于它变得更难维护,如果我们需要进行更改,我们需要在两个地方进行更改!
//
// 因此,我们改为: public MenuItem 使 Drink 成为 MenuItem 类的"派生类"(也称为"子类").
// Drink 类将被赋予其基类 MenuItem 的成员变量和成员函数,
// i.e.即我们的 Drink 对象也将具有公共成员变量 name、calories 和 print 功能!
//
// 我们仍然可以在 Drink 类中定义新的成员变量(如:ounces),以及新的成员函数(如:cal_per_ounce).
//
class Drink : public MenuItem
{
public:
  double ounces;//盎司
  
  double cal_per_ounce()
  {
    return calories / ounces;
  }
};

int main()
{
  // 实例化并使用 MenuItem 对象.
  MenuItem french_fries;
  french_fries.name = "French Fries";
  french_fries.calories = 400;
  french_fries.print();

  
  // 我们还可以实例化和使用 Drink 对象,
  // 使用相同的 name 和 calories 成员变量,以及相同的 print 成员函数,
  // 除了新的成员变量 calories 和新的成员函数 cal_per_ounce().
  //
  Drink hot_chocolate;
  hot_chocolate.name = "Hot Chocolate";
  hot_chocolate.calories = 300;
  hot_chocolate.ounces = 8;

  hot_chocolate.print();
  cout << "cal/ounce: " << hot_chocolate.cal_per_ounce() << endl;
  

  // 我们可以在任何可以使用基类的地方使用派生类,它会起作用!
  // 这是多态性的一个特性,另一个允许继承帮助我们编写更好的代码的概念.
  // 我们将 Drink 和 MenuItem 之间的关系描述为 “is a” 关系,因为 Drink 对象 “is a” MenuItem 对象.
  // Drink 对象可以在 MenuItem 对象可以使用的任何地方使用,因为 Drink 对象具有相同的成员变量和相同的可用成员函数.
  //
  // 这里我们创建了一个指向 MenuItem 变量的指针,并为它分配了 hot_chocolate Drink 对象实例的内存地址.
  // 这看起来很奇怪,但多态会允许这样做,我们甚至可以通过指针调用 drink 成员函数,就可以了.
  // 使这项工作的原因是我们 *知道* 一个 Drink 对象将有一个 print 对象,因为它也是一个 MenuItem.
  //
  MenuItem *ptr;
  ptr = &hot_chocolate;  
  ptr->print();
  
  return 0;
}