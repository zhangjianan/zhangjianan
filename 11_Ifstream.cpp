/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 11_Ifstream.cpp
 * @brief {演示如何使用ifstream从C++中读取文件.}
 ********************************************************************************/

#include <iostream>
#include <fstream>

using namespace std;

int main()
{
  // 取消注释/注释以下代码块并修改 file.txt 的内容
  // 以至于测试从文件中读取内容的不同示例.

  // 创建一个 ifstream 对象.
  ifstream inFile;

  // 尝试打开 file.txt 文件.
  inFile.open("./file/file2.txt");

  // 如果 file.txt 文件打开失败, fail() 将返回 true, 
  // 并且,我们将通过告诉用户出了什么问题,并以状态 1 
  // 退出,这是向 shell 发出的信号,表明程序执行的过程中有没有出错.
  if (inFile.fail())
  {
    cout << "Error opening file." << endl;
    return 1;
  }


  // 假设 file.txt 文件包含以下文本:
  // 4
  //

  // 将数字存储在文件中的变量(number).
  int number = 0;
  
  // 尝试读入 number.
  /*
  inFile >> number;

  // 如果我们尝试读取另一个数字(number),将会失败,可以使用 fail() 函数检查这一点. 

  inFile >> number;
  if (inFile.fail())
  {
    cout << "Error file format incorrect." << endl;
    return 1;
  }

  cout << number << endl;
  */

  // 尝试读取具有以下格式的文件:
  // 1.1 1.2 1.3
  // 2.1 2.2 2.3
  // 3.1 3.2 3.3
  //
  // 注意:我们如何读取多个值,
  // 还要注意:我们如何使用 eof() 函数来检测文件的结尾...
  // 这将允许我们处理具有任意行数的文件,  
  // 其中每行有 3 个双精度值.
  //

  /*
  double x1 = 0, x2 = 0, x3 = 0;
  while (true)
  {
    inFile >> x1 >> x2 >> x3;
    if (inFile.eof()) break;
    else 
      cout << x1 << " " << x2 << " " << x3 << endl;
  }
  */

  
  // 我们还可以将值存储至二维数组中,以供后续使用. 
  // 注意:我们如何在 while 循环条件中使用流提取操作符...
  // 提取成功时,将返回 true,否则返回 false(e.g. 例如:达到文件的末尾). 
  // 这是从文件中读取值的另一种方式. 
  // 还要注意:我们如何在循环的每次迭代中递增 i 以将值存储在 2D 数组的下一行.
  //
  /*
  double a[3][3];
  int i = 0;
  while (inFile >> a[i][0] >> a[i][1] >> a[i][2])
    i++;
  cout << endl;
  for (int j = 0; j < 3; j++)
  {
    cout << a[j][0] << " ";
    cout << a[j][1] << " ";
    cout << a[j][2] << endl;
  }
  */

  // 如果文件内容如下:
  // This is a string.
  // 
  // 那么,如果我们使用流提取操作符,只有关键字 "This" 将被存储到行中... 
  // 因为操作符将在第一个空白字符处停止.
  // 我们可以使用 getline() 来读取包括空格在内的整行,因为它将在文件中的第一个换行符处停止.
  //
  
  string line;
  // inFile >> line;
  getline(inFile, line);
  cout << line << endl;
  

  // 作为最佳的选择,我们在完成文件后关闭文件,它也具有释放一些内存的效果.
  inFile.close();

  return 0;
}