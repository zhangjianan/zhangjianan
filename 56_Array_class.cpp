/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-27
 * 
 * @file 56_Array_class.cpp
 * @brief {C++中使用数组类的例子.}
 ********************************************************************************/


#include <iostream>
#include <array>

using namespace std;

// 我们可以有(数组类函数参数)并将数组对象传递给函数.
void print_array(array<int,5> arr)
{
  cout << "Array: ";
  for (int element : arr)
    cout << element << " ";
  cout << endl;
}

int main()
{
  // 声明并初始化一个大小为 5 的数组类对象实例以存储提供的 int 值.
  // 数组类真的是一个模板类,因此 int 和 5 是模板参数,
  // 用于使用模板类创建特定于该类型 (int) 和大小 (5) 的数组类.
  array<int,5> array1 = {9,8,7,6,5};
  
  // 使用数组索引操作符(脚本操作符)输出索引 1 处的数组元素.
  cout << "array1[1] = " << array1[1] << endl;
  
  // 使用 at 成员函数输出索引为 2 处的数组元素.
  cout << "array1.at(2) = " << array1.at(2) << endl;
  
  // font() 成员函数会输出数组的第一个元素.
  cout << "array1.front() = " << array1.front() << endl;

  // back() 成员函数将输出数组中的最后一个元素.
  cout << "array1.back() = " << array1.back() << endl;
  
  // size() 成员函数会输出数组的大小(本例为 5),
  // 值得注意的是,编译器会将对 .size() 的调用替换为数组的大小作为常量值,
  // 所以使用 .size() 成员函数没有“函数调用性能开销”.
  cout << "array1.size() = " << array1.size() << endl;

  // max_size() 成员函数将返回与 size() 成员函数相同的大小.
  // 数组类是几个序列容器类之一,
  // 并且 max_size() 成员函数与其他序列容器类更相关. 
  cout << "array1.max_size() = " << array1.max_size() << endl;
  
  // 使用 size() 成员函数循环遍历并输出数组元素.
  for (int i = 0; i < array1.size(); i++)
    cout << "array1[" << i << "] = " << array1[i] << endl;
  
  // 声明另一个数组对象来存储 5 个整数.
  array<int,5> array2;
  
  // 使用 fill() 成员函数将所有 array2 元素设置为 10.
  array2.fill(10);
  
  // 循环遍历 array2 元素并输出它们以确认所有元素已设置为 10.
  for (int i = 0; i < array2.size(); i++)
    cout << "array2[" << i << "] = " << array2[i] << endl;
  
  // 使用 swap() 成员函数交换 array1 和 array2 的元素.
  array1.swap(array2);
  
  // 现在所有的 array1 元素都将设置为 10，这里我们使用基于范围的 for 循环来取出数组元素来验证这一点.
  cout << "Array1: ";
  for (int element : array1)
    cout << element << " ";
  cout << endl;
  
  // 输出所有 array2 元素以验证它们与交换前的旧 array1 元素匹配.
  cout << "Array2: ";
  for (int element : array2)
    cout << element << " ";
  cout << endl;
  
  // 如果数组大小为 0,则空成员函数将返回 true,否则返回 false,因此 array1 不会为空.
  if (array1.empty()) cout << "array1 is empty" << endl;
  else cout << "array1 is not empty" << endl;
  
  // 创建一个空数组.
  array<int, 0> array3;
  
  // 验证 empty() 成员函数可以检测到 array3 为空.
  if (array3.empty()) cout << "array3 is empty" << endl;
  else cout << "array3 is not empty" << endl;
  
  // 数组类在内部使用 C 风格的数组来存储数组数据,
  // 我们可以使用 data() 成员函数返回一个指向这个 C 风格数组的指针.
  // 然后我们可以使用[指针表示法]和/或[数组表示法]来直接访问数组元素.
  // 这里我们将指向 C 样式数组的指针（即内存地址）存储到 ptr.
  int *ptr = array1.data();

  // 使用[指针表示法]将索引 2 处的元素设置为 20.
  *(ptr + 2) = 20;

  // 使用[数组表示法]将索引 3 处的元素设置为 30.
  ptr[3] = 30;
  
  // 再次输出 array1 以验证上述分配是否正确.
  cout << "Array1: ";
  for (int element : array1)
    cout << element << " ";
  cout << endl;
  
  // 我们还可以将数组对象传递给函数.
  print_array(array1);
  
  // 数组类允许我们使用迭代器对象循环遍历数组.
  // begin() 成员函数将返回一个指向数组中第一个元素的迭代器,
  // 并且我们递增迭代器直到我们到达数组的末尾(end() 成员函数将返回一个迭代器指向数组中的最后一个元素).
  cout << "Array (iterator print): ";
  for (array<int,5>::iterator it = array1.begin(); it != array1.end(); it++)
  {
    // 解引用迭代器,并输出它指向的当前元素.
    cout << *it << " ";
  }
  cout << endl;

  // 上面循环中迭代器的类型比较复杂, 
  // 我们可以使用 auto 关键字,编译器将根据变量的初始化为 “it” 变量使用类型.
  // 这意味着如果我们稍后更改数组的大小,我们不需要更改此代码来匹配它.
  // 我们可以使用成员函数 rbegin() 和 rend() 反向遍历数组.
  // rbegin()、rend()、begin() 和 end() 函数也可以作为全局函数使用,正如下面使用. 
  cout << "Array (iterator print): ";
  for (auto it = rbegin(array1); it != rend(array1); it++)
  {
    cout << *it << " ";
  }
  cout << endl;
  
  // 如果我们在调试模式下编译程序,我们可以检测到数组越界错误,如下所示,
  // 我们尝试访问大小只有 5 个元素的数组中的第 6 个元素.
  //
  // array1[5] = 10;
  
  // 如果我们尝试访问超出范围的元素,at() 成员函数也会抛出异常.
  // 我们可以使用 try catch 块来处理这个异常,为我们提供了另一种处理这个问题的方法.
  // 这对于 C 风格的数组是不可能的.
  try
  {
    // 尝试访问超出范围的数组元素.
    array1.at(5);
  }
  // 捕获 out_of_range 异常并输出错误.
  catch (const out_of_range& orr)
  {
    cout << "ERROR OUT OF RANGE: " << orr.what() << endl;
  }
  
  return 0;
}