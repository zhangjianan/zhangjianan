/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-27
 * 
 * @file 58_New_delete.cpp
 * @brief {示例:在 C++ 中使用 new 和 delete 操作符.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 一个代表学生(Student)的简单类
class Student
{
public:

  // 学生对象有一个公共成员变量 name.
  string name;
  
  // 学生对象有一个打印学生 name 的公共成员函数 print.
  void print()
  {
    cout << name << endl;
  }
  
  // 定义一个构造函数,该构造函数根据提供的字符串参数设置学生姓名(name)成员变量.
  Student(string name) : name(name) {}
};


int main()
{
  // C++ 中的常规变量在栈(stack)上被赋予空间,
  // 内存中包含了固定大小的数据块并为程序员“自动”管理的空间.
  // 当我们在编写程序时知道程序将在编译时使用的“多少数据”时,栈(stack)就会起作用,
  // 我们可以简单地创建所需的变量 和/或 创建“足够大”的数组.
  int x = 4;
  
  // new 的操作符将在称为堆(heap)的不同内存部分中为数据分配空间.
  // 程序员需要管理堆(heap)上的数据, 
  // 当他们完成工作时,释放内存,使工作更具挑战性.
  // 但是有了堆(heap),我们可以在运行时,为我们需要的任何数据分配内存,因为我们的程序正在执行.
  // 这里我们在堆(heap)上为一个 int 分配空间,并将 int 设置为 5,
  // new 操作符将在我们存储在 ptr_to_int 变量中的堆(heap)上​​返回这个 int 的内存地址.
  // ptr_to_int 变量由 * 构成一个指针,它前面的 int 意味着它是一个指向 int 的指针.
  //
  int *ptr_to_int = new int(5);
  
  // 我们可以使用 * 操作符对指针进行解引用,并访问指针存储的内存地址...
  // 在这种情况下,我们将值 5 分配给内存中的这个空间.我们将其注释掉,因为我们已经将值初始化为 5.
  //
  // *ptr_to_int = 5;
  
  // 如果我们输出 ptr_to_int 我们将看到它是一个内存地址,如果我们解引用指针 ptr_to_int 我们将得到值 5.
  cout << "ptr_to_int: " << ptr_to_int << endl;
  cout << "*ptr_to_int: " << *ptr_to_int << endl;
  
  // 如果我们再次使用 new 操作符为 int 分配新空间,
  // 并将内存地址存储到 ptr_to_int 中,我们会有内存泄漏.
  // 这是因为不再可能释放 ptr_to_int 最初指向的堆(heap)上的内存...
  // 内存地址会“丢失”,我们在任何地方都没有它的副本.
  // 这意味着我们无法再将内存用于 delete 操作符...
  // 我们称之为“内存泄漏”,因为内存已经“消失”,我们无法再使用它.
  //
  // ptr_to_int = new int(10);
  
  // delete 操作符将释放由 new 操作符分配的堆(heap)上的内存,并使其可供我们的程序再次使用.
  // 当我们不再需要分配的内存时,我们使用 delete 操作符.
  delete ptr_to_int;
  

  // 我们还可以使用 [] 为一块内存分配空间,在这种情况下,一块内存可以存储 4 个双精度(double)值.
  // 分配的内存块将是连续的.  
  double *array = new double[4];
  
  // 我们可以像访问数据数组一样访问这块内存...在这种情况下设置 4 个双精度(double)值...
  array[0] = 5;
  array[1] = 6;
  array[2] = 7;
  array[3] = 8;
  
  // 我们也可以打印出动态分配的数据数组...
  for (int i = 0; i < 4; i++)
    cout << "array[" << i << "] = " << array[i] << endl;
  
  // 删除内存块时,我们需要再次使用 [],如下所示.
  delete[] array;
  

  // 我们也可以为对象动态分配空间,在这种情况下,我们在堆(heap)上存储一个 Student 对象.
  // 值得注意的是,C++ 起源于 C 语言,它使 malloc() 和 calloc() 函数可用于动态分配内存,这些函数在 C++ 中也可用.
  // 使用 malloc() 和 calloc() 与使用 new 操作符的最大区别在于,new 操作符在为堆(heap)上的对象分配空间时会调用对象的构造函数! 
  Student *student = new Student("Mary");
  
  // 我们可以使用 . 点操作符访问堆(heap)上对象的成员变量和函数只要我们使用 * 操作符解引用指向对象的指针.
  // 值得注意的是,我们需要将 *student 括在括号中,以确保在 . 点运算符(由于运算符优先级).
  // 
  (*student).name = "John";
  (*student).print();
  
  // 我们还可以使用 -> 箭头操作符来访问堆(heap)上对象的成员变量和函数.
  student->name = "Mary";
  student->print();
  
  // 我们可以使用 delete 关键字为堆(heap)上的学生(student)对象释放动态分配的内存.
  // C 语言中的函数 free() 在 C++ 中也可用,
  // 但与 new 操作符调用对象的构造函数的方式类似,delete 操作符则将调用对象的析构函数.  
  delete student;
  

  // 动态内存分配可能会失败, 
  // 例如,如果没有足够的内存可用或没有足够大小的连续块可用.
  // 我们可以使用 try-catch 块来检测和处理这种情况.
  try
  {
    // 尝试分配比可用空间多得多的空间,在这种情况下,new 运算符将抛出 bad_alloc 异常...
    double *big_array = new double[99999999999999];
  }
  catch (bad_alloc& exp)
  {
    // 捕获异常并输出错误信息...
    cout << "bad_alloc caught: " << exp.what() << endl;
  }
  
  // 如果我们不想使用异常,我们可以使用下面的 “nothrow” 和 new 运算符,而不是抛出异常,如果动态内存分配失败,new 将返回 NULL.
  double *big_again = new(nothrow) double[99999999999999];
  
  // 如果内存没有成功分配 NULL 将被分配给 big_again,
  // 所以如果 big_again 是 NULL 我们可以处理错误(在这种情况下只是输出错误消息).
  if (big_again == NULL)
    cout << "Failed to allocate again!" << endl;
  
  // 有一种通常称为占位符 new 操作符的东西,它允许重用以前分配的内存,它的用法如下所示:
  // 这是为了让读者了解这个概念,它将在以后的示例中更全面地介绍.
  // 在下面的示例中,第二个 double 值是在第一个语句中分配的内存空间中创建的(mydouble 存储的内存地址).
  //
  // double *mydouble = new double(12.2);
  // double *specific = new(mydouble) double(20.5);
  
  return 0;
}

// 内存的可视化...
//
// STACK|栈
// -----
//
//               内存
// Variable      Address      Value
//
// x             0x0001       4
// ptr_to_int    0x0002       0x9902
//               ...
//               ...
//               ...
// HEAP|堆       ...
// ----          ...
//               0x9901       5
//               0x9902       10
//               0x9903
//