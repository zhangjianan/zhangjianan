# zhangjianan
This is c++ Programming Tutorials
> Develop Environment：VSCode

1. 演示在 C++ 中打印 HelloWorld
2. 演示在 C++ 中使用 cout 的基础知识
3. 演示如何使用 cin 通过 C++ 接受用户的输入
4. 演示在 C++ 中使用字符串类型的基础知识
5. 演示一个基本的示例,旨在介绍 C++ 中的类、对象以及面向对象编程
6. 演示 C++ 中公共(public)和私有(private)访问说明符之间的区别
7. 演示在 C++ 中使用类成员函数(也称方法)的基础知识
8. 演示在 C++ 中使用构造函数(constructor)的基础知识
9. 演示在 C++ 中使用析构函数(destructor)的基础知识
10. 演示如何使用 ofstream 在 C++ 中写入文件
11. 演示如何使用 ifstream 从 C++ 中读取文件
12. 演示如何在 C++ 中使用类变量(也称为静态(static)成员变量)
13. 演示如何在 C++ 中使用静态(static)成员函数
14. 演示在 C++ 中声明和初始化对象数组的示例程序
15. 示例:在 C++ 中使用构造函数委托(construtor delegation),让一个构造函数调用同一个类中的另外一个构造函数,这有助于减少代码的重复
16. 演示如何在 C++ 中使用按引用(reference)传递.通过引用传递允许函数修改它在调用函数中提供的参数,这与按值传递相反
17. 演示对象赋值如何在 C++ 中工作,包括一个例子:如果我们的对象包含指向堆(heap)上数据的指针的成员变量,为什么我们需要小心对象分配
18. 演示在 C++ 中创建和使用复制构造函数(copy constructor)的示例
19. 演示在 C++ 中使用函数重载在同一作用域内拥有多个同名函数的示例
20. 演示如何在 C++ 中使用 setw 流操纵器将数据输出到具有指定字符宽度的字段中
21. 演示在 C++ 中使用 setprecision 流操纵器格式化浮点值输出的示例
22. 演示 C++ 中的运算符重载示例
23. 示例:在 C++ 中实现方法链接(method chaining)
24. 演示 C++ 中 this 关键字的几个用例演示
25. 演示在 C++ 中使用引用变量的示例
26. 演示在 C++ 中如何、为什么以及何时使用成员初始化器列表的示例
27. 演示在 C 中给文件添加行号
28. 演示如何在 C++ 中使用继承(inheritance)的基本演示
29. 演示 C++ 中的多级继承演示
30. 演示构造函数(constructors)如何在 C++ 中使用继承(inheritance)
31. 演示当我们有一个基类和一个派生类时,析构函数(destructors)如何在 C++ 中使用继承(inheritance)的示例
32. 演示 C++ 中的多重继承(multiple inheritance)以及可能出现的一些问题,包括菱形问题
33. 演示受保护(protected)的访问说明符如何与 C++ 中的类成员一起使用
34. 演示基类访问说明符如何与 C++ 中的继承一起使用
35. setbase
36. range based for loop
37. default arguments
38. clear input buffer
39. convert class to primitive
40. ternaryOp Lvalue
41. swap function
42. dynamic polymorphism
43. virtual destructors
44. sort function
45. namespaces
46. abstract class
47. Pure virtual  destructor
48. 1
49. Implement_pure_virtual_function
50. 演示在 C++ 中使用函数模板减少代码重复的示例
51. 演示在 C++ 中使用类模板的示例
52. 演示 C++ 中如何重载函数模板的例子,结合函数重载的好处和函数模板的好处
53. 演示如何在 C++ 中使用友元函数的示例
54. Operator_overloading_friend_function
55. Const_array_parameters
56. 演示 C++ 中使用数组类的例子
57. 演示使用 C++ 中的 unique 函数删除范围内连续重复的示例
58. 演示示例:在 C++ 中使用 new 和 delete 操作符