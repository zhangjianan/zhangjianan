/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 32_Multiple_inheritance.cpp
 * @brief {演示 C++ 中的多重继承以及可能出现的一些问题,包括菱形问题.}
 ********************************************************************************/

// 继承说明
//
//
//                    CommonBaseClass
//                    /              \
//                   /                \
//                  /                  \
//              BaseClass1           BaseClass2
//                   \                /
//                    \              /
//                     \            /
//                      DerivedClass
//
//
// 下面定义的类有如下关系,
// DerivedClass 使用多重继承从 BaseClass1 和 BaseClass2 继承.
// 但是我们也会有一个 BaseClass1 和 BaseClass2 都继承自的 CommonBaseClass,从而导致"菱形问题"的实例.  
//
// "◇菱形问题"的维基百科文章: 
// https://en.wikipedia.org/wiki/Multiple_inheritance#The_diamond_problem
//

#include <iostream>
using namespace std;

// BaseClass1 和 BaseClass2 都将从中继承的公共基类(CommonBaseClass).
class CommonBaseClass
{
public:
  int common_value;
  
  // 定义一个默认和参数化的构造函数.
  CommonBaseClass() : common_value(-99) {}
  CommonBaseClass(int common_value) : common_value(common_value) {}
};

// BaseClass1 使用 virtual 关键字从 CommonBaseClass 继承, 
// 这将防止下面的 DerivedClass 在从 BaseClass1 和 BaseClass2 继承时,继承两个 CommonBaseClass common_value 成员变量!
class BaseClass1 : virtual public CommonBaseClass
{
public:
  // BaseClass1 和 BaseClass2 都有一个成员变量 value.
  int value;
  
  // 让 BaseClass1 使用 value 为 100 的 CommonBaseClass 的参数化构造函数.
  BaseClass1() : CommonBaseClass(100) {}
  
  // BaseClass1 有一个成员函数 function1,它也不同于 BaseClass2 的 function1.
  void function1()
  {
    cout << "BaseClass1' Function1()" << endl;
  }
};

// BaseClass2 也继承了 CommonBasClass 使用 virtual 关键字,
// 来防止下面的 DerivedClass 在继承自 BaseClass1 和 BaseClass2 时,继承两个 CommonBaseClass common_value 成员变量
class BaseClass2 : virtual public CommonBaseClass
{
public:
  // 与 BaseClass1 同名的成员变量.
  int value;
  
  // BaseClass2 也调用 CommonBaseClass 的参数化构造函数,但参数为 200.
  BaseClass2() : CommonBaseClass(200) {}

  // BaseClass2 还有一个名为 function1 的成员函数.
  void function1()
  {
    cout << "BaseClass2' Function1()" << endl;
  }
};

// DerivedClass 使用多重继承特性从 BaseClass1 和 BaseClass2 继承.
class DerivedClass: public BaseClass1, public BaseClass2
{
public:
  
  // 当 BaseClass 从 BaseClass1 和 BaseClass2 继承时,它将有两个 function1 成员函数,一个与每个基类相关联! 
  // 
  // 我们可以通过几种方式处理这种情况:
  //
  // DerivedClass 可以覆盖两个 function1 定义,
  // 如果 DerivedClass 需要此函数来执行与任一 BaseClass 不同的操作,这将是一个合适的解决方案.
  //
  // 正如下面注释的那样,我们可以覆盖 function1,但让函数只调用 BaseClass1 或 BaseClass2 的 function1 定义, 
  // 有效地让 DerivedClass 继承两个函数定义中的 "一个" 作为解决歧义的一种方式.
  // 如果 DerivedClass 只需要基类的 functions 之一,这将是一个合适的解决方案.
  //
  // 如果我们需要派生类访问两个基类的函数定义,
  // 然后,我们可以像下面的 main 函数一样，使用作用域解析运算符 :: 根据需要调用每个函数.
  //  
  /*
  void function1()
  {
    BaseClass1::function1();
  }
  */
  
  // 我们可能会认为,因为 DerivedClass 继承自 BaseClass1 和 BaseClass2,
  // 所以它将继承 BaseClass1 和/或 BaseClass2 如何构造 CommonBaseClass 对象.
  // 但是对于多重继承,这不会发生,而是这个 DerivedClass 负责构造 CommonBaseClass,这可以防止任何歧义.
  //
  DerivedClass() : CommonBaseClass(999) {}
    
};

int main()
{
  // 创建 DerivedClass 对象实例.
  DerivedClass derived;
  
  // 当我们输出 common_value 成员变量,
  // 我们可能期望它将是 100 或 200,由 BaseClass1 和 BaseClass2 调用的 CommonBaseClass 构造函数设置.
  // 但是相反,DerivedClass 调用了值为 999 的 CommonBaseClass 构造函数.
  // 这可能令人惊讶,因为派生类通常会调用基类的构造函数,
  // 但是在这种具有多重继承和 virtual 关键字的情况下,DerivedClass 将决定如何构造 CommonBaseClass!
  cout << "Common value: " << derived.common_value << endl;
  
  // 在使用多重继承的派生类的基类共享成员变量名的情况下,
  // 派生类将获得两个同名的成员变量,我们可以使用 ClassName 和 :: 作用域歧义运算符来解决这个歧义,即正在访问哪个.
  // 如果我们不这样做会导致错误.
  derived.BaseClass2::value = 20;
  
  // 我们可以用同样的方法解决 function1 成员函数之间的歧义...
  derived.BaseClass1::function1();
  derived.BaseClass2::function1();

  // 如果我们取消注释 DerivedClass 中的 function1() 定义,
  // 并注释掉上面的两个语句,
  // 然后我们将覆盖 function1() 并更简单地调用它,如下面的代码...
  // 在这种情况下,被覆盖的函数已设置为专门调用 function1 的 BaseClass1 定义.
  //
  // derived.function1();

  return 0;
}
