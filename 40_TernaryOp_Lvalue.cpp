/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-27
 * 
 * @file 40_TernaryOp_Lvalue.cpp
 * @brief {C++ 中一个鲜为人知的特性是我们可以使用三元运算符作为左值,在这个程序中,我们以赋值语句左侧的三元运算符为例.}.}
 ********************************************************************************/

#include <iostream>

using namespace std;

int main()
{
  // 程序将接受成绩(grade)输入,跟踪及格(pass)和不及格(fail)的成绩.
  int grade = 0;
  int pass_total = 0;
  int fail_total = 0;
  
  // 继续“永远”接受成绩输入.
  while (true)
  {
    // 提示用户输入等级(grade),将用户输入存储到等级(grade)变量中.
    cout << "Enter Grade (-1 To Quit): ";
    cin >> grade;
    
    // 如果用户输入 -1 退出.
    if (grade == -1) break;
    
    // 这里我们使用三元运算符作为左值,如果等级大于等于 50 我们将 pass_total 加 1,否则我们将 fail_total 加 1...
    // 这是 C++ 的一个“隐藏特性”,也许它鲜为人知,因为我们不能对 C 中的三元运算符做同样的事情!
    (grade >= 50 ? pass_total : fail_total) += 1;
    
    // 输出通过和不及格的总成绩.
    cout << "pass_total: " << pass_total << endl;
    cout << "fail_total: " << fail_total << endl;
  }
  
  return 0;
}