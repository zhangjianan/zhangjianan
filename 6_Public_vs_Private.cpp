/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 6_Public_vs_private.cpp
 * @brief {演示 C++ 中公共(public)和私有(private)访问说明符之间的区别.}
 ********************************************************************************/

#include <iostream>
#include <string>

using namespace std;

// 定义一个员工(Employee)类
class Employee
{

// 公共(public)访问说明符将使得下面定义的成员变量和函数可用于在类外部访问
// (例如:在下面的主函数中)
public:

  // 公共(public)成员变量:员工(Employee)的姓名
  string name;
  
  // 通常定义公共(public)的 "getter 与 setter" 函数来管理访问 
  // 私有(private)成员变量,比如:薪水(salary) (如下)
  void set_salary(double potential_salary)
  {
    // setter 与 getter 函数可以"保护"和"管理"对成员的访问,
    // 例如,在这种情况下,在设置薪水(salary)之前应用一些错误处理的逻辑.... 
    // 如果提供的薪水(salary)为负数,我们默认将薪水(salary)设置为 0.
    if (potential_salary < 0) salary = 0;
    else salary = potential_salary;
  }
  
  // 返回薪水(salary)的 getter 成员函数.
  double get_salary()
  {
    return salary;
  }
  
  // 打印员工的姓名(name)和奖金(bonus)
  void print_bonus()
  {
    // 我们可以在类中访问私有(private)成员函数,例如:calculate_bonus
    cout << name << "' bonus: " << calculate_bonus() << endl;
  }
  
// 私有(private)访问说明符限制这些成员变量和函数,使得它们只能在类内部
// 访问,而不能被"外部世界"(例如:主函数)访问.
private:

  // 私有(private)成员变量:员工(Employee)的工资
  double salary;
  
  // 私有(private)成员函数:计算员工的奖金(bonus)
  double calculate_bonus()
  {
    return salary * 0.10;
  }
  
};

int main()
{
  // 创建一个员工(employee)对象.
  Employee employee1;

  // 我们可以访问公共(public)成员变量名
  employee1.name = "zhangJA";
  cout << employee1.name << endl;
  
  // 我们也可以访问公共(public)成员函数
  employee1.set_salary(50000);
  cout << "salary: " << employee1.get_salary() << endl;
  employee1.print_bonus();

  // 如果我们尝试访问类之外的私有成员,将会得到一个编译器错误结果,如:
  // employee1.salary = 20000;
  // employee1.calculate_bonus();
  
  return 0;
}