/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 23_Method_chaining.cpp
 * @brief {示例:在 C++ 中实现方法链接.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 基本的 Rectangle 类,但带有允许方法链接的 setter 方法.
class Rectangle
{
public:
  
  // rectangles 有 length、width、color 成员变量.
  int length;
  int width;
  string color;
  
  // 用于初始化成员变量的标准构造函数.
  Rectangle(int set_length, int set_width, string set_color)
  {
    length = set_length;
    width = set_width;
    color = set_color;
  }
  
  // 打印成员变量值的标准函数.
  void print()
  {
    cout << "Length: " << length << endl;
    cout << "Width: " << width << endl;
    cout << "Color: " << color << endl;
  }
  
  // 将 color 成员变量设置为作为参数提供的值的 setter 方法,
  // 但是该方法还返回对调用它的对象实例的引用,从而允许方法链接.
  Rectangle& setColor(string set_color)
  {
    // 设置成员变量
    color = set_color;

    // 这是一个指向执行此方法的对象实例的指针,
    // 当我们用 * 解引用指针时,我们返回对对象实例本身的引用.
    return *this;//this:0x79fd80
  }
  
  // 同上,对 length 成员变量设置.
  Rectangle& setLength(int set_length)
  {
    length = set_length;
    return *this;
  }
  
  // 同上,对 width 成员变量设置.
  Rectangle& setWidth(int set_width)
  {
    width = set_width;
    return *this;
  }
  
};

int main()
{
  // 创建一个 rectangle 对象.
  Rectangle rectangle1(4,5,"red");
  
  // 将方法链接与 setter 方法一起使用.
  //
  // 当我们调用:
  //    rectangle1.setColor("orange")
  //
  // 返回的是对 rectangle1 的引用,所以当我们在这个上运行 .setLength(10) 时,它实际上就像在运行:
  //
  // rectangle1.setLength(10)
  //
  // (运行 rectangle1.setColor("orange") 后)
  //
  // setWidth(2) 等等.
  //
  rectangle1.setColor("orange").setLength(10).setWidth(2);
  
  // 打印 rectangle 以便我们可以看到 setter 方法的影响.
  rectangle1.print();
  
  return 0;
}

// 假设我们有一个对象...
//
// ObjectType object1;
//
//
// 我们可以这样调用方法...
//
// object1.method1();
// object1.method2();
// object1.method3();
//
// - 但是我们重复 object1 三遍.
//
//
// 方法链接:
//
// object1.method1().method2().method3();
//
// - object1 不再重,
// - 我们可以从左到右阅读.
//
//
// 方法链接是否值得争论,我将发布一个链接,指向一篇关于该主题的有趣论文.
//
// Paper: "An Empirical Study of Method Chaining in Java"
//
// Link: https://static.csg.ci.i.u-tokyo.ac.jp/papers/20/nakamaru-msr2020.pdf
//
// 在我们拥有作用于对象的方法链接的情况下,它似乎很有用,例如:setter 方法.
//
// 它广泛用于 jQuery,一个非常流行的 JavaScript 库.
//