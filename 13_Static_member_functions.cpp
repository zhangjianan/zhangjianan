/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 13_Static_member_functions.cpp
 * @brief {演示如何在C++中使用静态(static)成员函数.}
 ********************************************************************************/


#include <iostream>

using namespace std;

// 简单的方形(Square)类型
class Square
{
  
private:
  int side_length;

  // 静态(static)成员变量,也称为类变量, 
  // 是类的变量,而不是特定于单个对象实例.
  static int total_squares;
  
  // 静态(static)成员函数可以是私有的,并且只能在类中访问.
  static void incrementTotal()
  {
    // 静态(static)成员函数可以改变静态(static)成员变量.
    total_squares++;
  }
  
public:
  
  Square(int length)
  {
    side_length = length;

    // 我们可以在非静态(non-static)成员函数中调用私有静态(private static)成员函数.
    incrementTotal();
  }
    
  int area()
  {
    return side_length * side_length;
  }
  
  // 公共静态(public static)成员函数在类定义之外可用,
  // 比如在主函数中.
  static int getTotal()
  {
    return total_squares;
  }
  
};

// 初始化私有成员变量.
int Square::total_squares = 0;

int main()
{
  Square square1(5);
  
  cout << "square1 area: " << square1.area() << endl;
  
  // 我们可以使用语法 ClassName::FunctionName() 访问 getTotal() 静态成员函数.
  cout << "total squares: " << Square::getTotal() << endl;
  
  Square square2(10);
  
  cout << "square2 area: " << square2.area() << endl;
  
  // 我们还可以使用任何特定的对象实例访问 getTotal() 静态成员函数,
  // 但是,最好使用 ClassName::FunctionName() 的语法.
  cout << "total squares: " << square2.getTotal() << endl;

  return 0;
}