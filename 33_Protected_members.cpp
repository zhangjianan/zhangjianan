/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 33_Protected_members.cpp
 * @brief {演示受保护(protected)的访问说明符如何与 C++ 中的类成员一起使用.}
 ********************************************************************************/

#include <iostream>
using namespace std;

// 具有公共(public)、私有(private)和受保护(protected)成员变量的基类(BaseClass).
class BaseClass
{
public:
  int _public_member;
  
private:
  int _private_member;
  
protected:
  int _protected_member;
};

// 从基类继承的派生类.
class DerivedClass : public BaseClass
{
public:
  void member_function()
  {
    // 派生类(DerivedClass)可以访问基类(BaseClass)的公共(public)成员,就像程序的其余部分一样.
    _public_member = 10;

    // 派生类(DerivedClass)不能访问基类(BaseClass)的私有(private)成员,就像程序的其余部分一样(如果未注释,以下语句将导致编译器错误).
    // _private_member = 20;

    // 但是,派生类(DerivedClass)可以访问基类(BaseClass)的受保护(protected)成员,这与没有访问权限的程序的其余部分不同.
    _protected_member = 30;
    
    // 输出刚刚设置的值.
    cout << "_public_member: " << _public_member << endl;
    cout << "_protected_member: " << _protected_member << endl;
    // cout << "_private_member: " << _private_member << endl;
  }
};

// 这里我们定义继承自上面的 DerivedClass 的 AnotherClass.
class AnotherClass : public DerivedClass
{
public:
  void new_function()
  {
    // 上面的 DerivedClass 会在使用公共(public)访问说明符从 Class 继承时继承受保护(protected)的成员作为受保护(protected)的成员,
    // 即：类 DerivedClass ：公共 BaseClass, 
    // 所以当我们在这里继承另一个类中的 DerivedClass 时,同样使用公共(public)访问说明符,我们可以再次访问受保护(protected)的成员.
    _protected_member = 20;
  }
};

int main()
{
  BaseClass base;

  // 程序的其余部分可以访问基类公共(public)成员. 
  base._public_member = 10;

  // 程序的其余部分不能访问基类私有(privated)成员.
  // base._private_member = 20;
  
  // 程序的其余部分也不能访问基类受保护(protected)的成员,只有派生类可以.
  // base._protected_member = 30;

  DerivedClass derived;
  
  // 测试我们的 function 以确保我们真的在访问受保护(protected)的成员.
  derived.member_function();

  return 0;
}