/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-27
 * 
 * @file 41_Swap_function.cpp
 * @brief {在 C++ 标准库中使用 swap() 函数的示例.}
 ********************************************************************************/

#include <iostream>

// 我们可以通过包含实用程序来访问 swap() 函数
#include <utility>

using namespace std;

// 一个简单的类
class Data
{
public:
  int value;
};

int main()
{
  int a = 2;
  int b = 7;
  
  // 交换前输出 a 和 b
  cout << "a: " << a << " b: " << b << endl;
  
  // 我们可以使用 swap 来交换原始类型,例如 int 值.
  swap(a,b);
  
  // 通过交换后输出 a
  cout << "a: " << a << " b: " << b << endl;

  
  int x[] = {1,2,3};
  int y[] = {4,5,6};
  
  // 输出交换前的 x[] 和 y[] 数组值.
  cout << "x[]: " << x[0] << "," << x[1] << "," << x[2] << endl;
  cout << "y[]: " << y[0] << "," << y[1] << "," << y[2] << endl;
  
  // 我们也可以使用 swap 来交换数组值
  swap(x,y);
  
  // 交换后输出 x[] 和 y[] 数组值
  cout << "x[]: " << x[0] << "," << x[1] << "," << x[2] << endl;
  cout << "y[]: " << y[0] << "," << y[1] << "," << y[2] << endl;

  
  // 创建两个 Data 对象实例并设置 value 成员变量.
  Data dataX, dataY;
  dataX.value = 2;
  dataY.value = 7;
  
  // 输出交换前 dataX 和 dataY 的 value 成员变量值
  cout << "X: " << dataX.value << " Y: " << dataY.value << endl;
  
  // swap 也可以交换对象实例.
  swap(dataX, dataY);
  
  // 输出交换后 dataX 和 dataY 的 value 成员变量值
  cout << "X: " << dataX.value << " Y: " << dataY.value << endl;

  return 0;
}