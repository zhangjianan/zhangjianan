/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-27
 * 
 * @file 57_Unique.cpp
 * @brief {使用 C++ 中的 unique 函数删除范围内连续重复的示例.}
 ********************************************************************************/

#include <iostream>
#include <vector>

// unique() 在 algorithm(算法) 库中，所以我们应该包含这个库,
// 虽然当我们包含向量时它也可能包含在内.
#include <algorithm>

using namespace std;

// 用于确定范围中的 2 个元素何时等效的自定义函数,
// 我们可以选择将此函数作为第 3 个参数传递给 unique,它将改变函数的默认行为.
// 如果 2 个元素被认为是等效的,则该函数应返回 true,否则返回 false,
// 在这种情况下,我们仅在 n1 等于 n2 并且两个数字都小于 10 时才返回 true.
bool match(int n1, int n2)
{
  return n1 == n2 && n1 < 10 && n2 < 10;
}

int main()
{
  // 在这个向量(vector)中,只有 1,1 和 3,3 和 12,12 是连续重复的,'2' 元素是重复的但不是连续重复的.
  vector<int> data = { 9,1,1,2,3,3,2,5,12,12 };
  
  // 在调用 unique 函数之前输出向量,使用迭代器循环遍历向量元素.
  cout << "Before: ";
  for (auto it = data.begin(); it != data.end(); it++)
    cout << *it << " ";
  cout << endl;
  
  // 输出前面向量的大小.
  cout << "Before Size: " << data.size() << endl;
  
  // 调用 unique 函数并将迭代器传递到我们希望从中删除连续重复的范围的开始和结束(技术上是结束后的一个元素).
  // unique 函数将返回一个迭代器,该迭代器指向最后一个不被删除的元素(即过滤和可能缩短的范围的“结束”).
  auto new_end = unique(data.begin(), data.end());
  
  // 我们可以选择提供第 3 个参数,用于确定元素何时被视为等效的自定义函数.
  // 我们使用我们的 match 函数,它只会在两个元素 < 10 的情况下找到相等的两个元素,
  // 这将具有不删除第 2 个 12 元素的效果.
  // 注释掉上面对 unique 的调用并取消注释下面的函数调用来测试它...
  //
  // auto new_end = unique(data.begin(), data.end(), match);
  
  // 如果我们的过滤范围小于原始范围,
  // “new end”和“old end”之间的元素不再需要,因此我们调整向量的大小以删除它们.
  data.resize(distance(data.begin(), new_end));
  
  // 调用 unique 并重新调整向量大小后输出向量的大小.
  cout << "After Size: " << data.size() << endl;
  
  // 之后也输出向量内容.
  cout << "After All: ";
  for (auto it = data.begin(); it != data.end(); it++)
    cout << *it << " ";
  cout << endl;
  
  return 0;
}