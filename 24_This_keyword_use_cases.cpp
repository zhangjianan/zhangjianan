/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 24_This_keyword_use_cases.cpp
 * @brief {C++ 中 this 关键字的几个用例演示.}
 ********************************************************************************/

#include <iostream>
using namespace std;

// 声明稍后我们将定义的 Student 类.
class Student;

// 声明 create_report 函数,它将使用指向 Student 对象的指针作为参数. 
void create_report(Student *student);

// 定义一个简单的 Student 类,它允许我们演示这个的用例.
class Student
{
public:
  // Student 对象将有两个成员变量,分别是学生 name 和 age.
  string name;
  int age;
  
  // 构造函数使用与成员变量同名的参数!
  // 如果我们试图分配:age = age;这不起作用,我们会有一个错误,但我们可以使用 this 关键字来访问成员变量 age.
  // 这将允许我们设置成员变量,但它也允许使用具有相同名称的参数而不会出现问题...
  // 这是 this 关键字的一个潜在用例.
  Student(string name, int age)
  {
    // "this"是提供给每个非静态(non-static)成员函数的隐式参数,包括构造函数,它是指向调用此成员函数的对象实例的指针...
    // 请注意:当我们输出 this(指针,即成员地址)时,它的值与我们在 main 函数中输出 student1 的地址时的值相同...
    // 那是因为它们是一样的!
    cout << "    this 的内存地址: " << this << endl;
    
    // 我们可以将指针 this 与箭头 -> 结合使用来访问正在运行成员函数的对象实例的成员变量.
    this->name = name;
    this->age = age;
  }
  
  // 我们可以在任何非静态(non-static)成员函数中使用它,而不仅仅是构造函数,就像我们在这里访问 age 成员变量并将其递增 1 一样.
  void increase_age()
  {
    this->age = this->age + 1;
  }
  
  // 我们还可以使用指针 this 访问对象实例的成员函数....
  // 这里我们用它来调用 increase_age() 成员函数!
  void increase_and_output_age()
  {
    this->increase_age();
    cout << "age: " << this->age << endl;
  }
  
  // 我们调用 create_report student 函数并将指针传递给调用 graduate 成员函数的对象实例...
  // 即我们本质上是让对象"将指向自身的指针"传递给函数!
  // 这是 this 指针的另一个用例.
  void graduate()
  {
    cout << "Congratulations!" << endl;
    create_report(this);
  }
  
  // 我们也可以使用 this 指针来实现方法链接...
  // 通过让成员函数返回对调用它们的对象的引用,
  // 我们可以将一系列成员函数(即方法)调用链接在一起,就像我们在 main 中使用 set_name 和 set_age 所做的那样.
  Student& set_name(string name)
  {
    // 将 name 成员变量设置为提供的参数.
    this->name = name;

    // 解引用指向调用此成员函数的对象的指针,
    // 当与返回类型 Student& 一起使用时,我们将返回对调用此成员函数的对象的引用...
    // 允许使用返回值调用另一个成员函数.
    return *this;
  }
  
  // 与上面的 set_name 函数相同的想法,同理 age 成员变量设置
  Student& set_age(int age)
  {
    this->age = age;
    return *this;
  }
};

// 创建关于学生的"报告"...
// 我们实际上可能不希望将此逻辑包含在 Student 类本身的成员函数中,这取决于我们如何拆分程序的功能.
// 从体系结构的角度来看,最好从类(class)外部处理创建关于学生的报告,在这种情况下,可以使用这种方法.
void create_report(Student *student)
{
  cout << student->name << " " << student->age << endl;
}

int main()
{
  // 创建 student 对象,构造函数会输出"this"指针内存地址 
  Student student1("Lucas", 20);
  
  // 注意:student1 的内存地址如何与"this"指针内存地址相同...
  // 那是因为这正是 "this" 指针应该是的...
  // 指向正在为其调用成员函数的对象实例的指针.
  cout << "student1 的内存地址: " << &student1 << endl;
  
  // 增加学生年龄并输出年龄.
  student1.increase_and_output_age();
  
  // 使用方法链使用 seter 方法设置学生的姓名和年龄.
  // 使这项工作有效的原因是此成员函数在此处调用:
  //
  // student1.set_name("John")
  //
  // 将返回一个 *引用* 到 student1 对象,这意味着可以使用此"返回值"调用 .set_age(23) 并继续处理同一个对象.
  //
  // 值得注意的是,因为这两个 setter 方法都返回对调用它们的对象的引用,所以我们可以切换它们的顺序,它仍然可以工作.
  student1.set_name("John").set_age(23);
  
  // 调用将对象实例的指针传递给 create_report 函数的 gradute 成员函数以创建报告.
  student1.graduate();
  
  return 0;
}