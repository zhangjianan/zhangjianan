/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 34_Base_class_access_specifiers.cpp
 * @brief {演示基类访问说明符如何与 C++ 中的继承一起使用.
 *         基类访问说明符与基类成员访问说明符一起确定派生类中成员的访问说明,
 *         在这段代码中,我们通过示例了解了不同的组合.}
 ********************************************************************************/

//  此表显示了哪个访问说明符将产生派生类,基类访问说明符和成员访问说明符在基类中的组合.
// 
//
//                    *                   成员访问级别
//  基类访问说明符-->  *   Private          Protected       Public
//  *****************************************************************
//                    *
//  Public            *   Inaccessible     Protected       Public
//                    *
//  Protected         *   Inaccessible     Protected       Protected
//                    *
//  Private           *   Inaccessible     Private         Private
//     ^
//     |
//  派生类访问说明符
//  
//  基类访问说明符将影响派生类继承成员的方式.
//  例如,在继承基类时使用保护访问说明符时,基类的公共成员将成为派生类的保护成员.
//  通常,我们使用公共(public)说明符.值得注意的是,如果没有提供访问说明符,则默认为私有(private).
//

#include <iostream>
using namespace std;

// 具有公共(public)、受保护(protected)和私有(private)成员的简单 BaseClass 基类
class BaseClass
{
public:
  int _public_member;
  
protected:
  int _protected_member;
  
private:
  int _private_member;
};

// 使用 Public 基类访问说明符的派生类.
class DerivedClass1 : public BaseClass
{
public:
  void member()
  {
    // 基类的私有(private)成员在派生类中将不可访问(Inaccessible).
    // _private_member = 20;
    
    // 基类的受保护(protected)成员将成为派生类的受保护(protected)成员,因此可在派生类中访问.
    _protected_member = 30;
    
    // 基类的公共(public)成员将成为派生类的公共(public)成员.
    _public_member = 10;
  }
};

// 使用 Protected 基类访问说明符的派生类.
class DerivedClass2 : protected BaseClass
{
public:
  void member()
  {
    // 基类的私有(private)成员将在派生类中变得不可访问(Inaccessible).
    // _private_member = 30;
  
    // 基类的受保护(protected)成员将成为派生类中的受保护(protected)成员.
    _protected_member = 20;

    // 基类的公共(public)成员也将成为派生类中的受保护(protected)成员.
    _public_member = 10;
  }
};

// 使用 Private 基类访问说明符的派生类.
class DerivedClass3 : private BaseClass
{
public:
  void member()
  {
    // 基类的私有(private)成员将在派生类中变得不可访问(Inaccessible).
    // _private_member = 30;

    // 基类的受保护(protected)成员和公共(public)成员都将成为派生类中的私有(private)成员.
    _protected_member = 10;
    _public_member = 20;
  }
};

// 如果我们创建一个 DerivedClass3 的派生类,
// 我们发现我们无法访问 _protected_member 和 _public_member,
// 因为当 DerivedClass3 使用私有(private)基类访问说明符从 BaseClass 继承它们时,它们都是私有(private)成员!
class NextLevel : public DerivedClass3
{
public:
  void member()
  {
    // _protected_member = 20;
    // _public_member = 30;
  }
};


int main()
{
  // 创建 DerivedClass1 的实例,以根据成员变量的访问规范确认成员变量的通常行为.
  DerivedClass1 derived1;
  
  // 我们无法访问类之外的受保护(protected)成员,就像受保护成员的通常情况一样.
  //
  // derived1._protected_member = 30;
  
  // 我们可以访问类外的公共(public)成员,就像公共成员的通常情况一样.
  derived1._public_member = 10;
  
  /*********************************************************************************/
  // 创建 DerivedClass2 的实例,以根据成员变量的访问规范确认其通常行为.
  DerivedClass2 derived2;
  
  // 我们无法访问类之外的受保护成员,就像受保护成员的通常情况一样.
  //
  // derived2._protected_member = 30;
  
  // _public_member 现在已经变成了 DerivedClass2 中的受保护成员,
  // 因为我们使用了受保护的基类访问说明符,因此我们不能再在 DerivedClass2 之外访问它.
  //
  // derived2._public_member = 30;

  /*********************************************************************************/
  // 创建 DerivedClass3 的实例,以根据成员变量的访问规范确认成员变量的通常行为.
  DerivedClass3 derived3;
  
  // 我们无法访问 _public_member 或 _protected_member,
  // 因为当我们在继承 BaseClass 时使用私有(private)基类访问说明符时,它为这两个变量提供了私有访问说明.
  //
  // derived3._public_member = 20;
  // derived3._protected_member = 30;
  
  return 0;
}