/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 36_Range_based_for_loop.cpp
 * @brief {在 C++ 中使用基于范围的 for 循环的示例.}
 ********************************************************************************/

#include <iostream>
#include <array>

using namespace std;

// 我们不能将基于范围的 for 循环与这样的数组函数参数一起使用，因为在 C++ 中，数组参数是 *really* 指针
// i.e.(即)数组的内存地址被传递给函数.
// 有一个潜在的解决方法,但它有点复杂：https://stackoverflow.com/a/31824397
/*
void func1(int array[])
{
  for (int element : array)
  {
    cout << element << endl;
  }
}
*/

int main()
{
  int array[] = {1,2,3,4,5,6,7,8,9,10};

  // 循环体将按顺序对数组中的每个元素执行,并且元素变量将设置为数组的每个元素.
  for (int element : array)
  {
    cout << element << endl;
  }
  
  // 例如:我们可以将基于范围的 for 循环与任何范围表达式一起使用,而不仅仅是变量.
  for (int element : {1,2,3,4,5})
  {
    cout << element << endl;
  }

  // 我们可以将基于范围的 for 循环用于任何范围,例如向量或数组容器类...
  // 使用数组容器类时请注意,基于范围的 for 循环将根据数组 SIZE 对数组中的每个元素执行(在以下情况下为 50)
  // 而不是已经初始化的元素数量(在下面的例子中:5)
  array<int, 50> array_obj = {1,2,3,4,5};
  
  // 循环将输出我们初始化的前 5 个元素,然后是剩余元素的额外 45 个 0.
  for (int element : array_obj)
  {
    cout << element << endl;
  }
  
  // 创建一个测试字符串
  char string[] = "some characters in a string!";
  
  // 使用基于范围的 for 循环计算字符串中 s 字符的数量... 
  // 任何我们需要为范围内的每个元素做某事的情况都是使用基于范围的 for 循环的合适情况.
  int s_count = 0;
  for (char c : string)
  {
    // 每当找到“s”字符时,递增计数,最初为 0
    if (c == 's') s_count++;
  }

  // 输出 s 个字符的个数
  cout << "s count: " << s_count << endl << endl;
  
  return 0;
}