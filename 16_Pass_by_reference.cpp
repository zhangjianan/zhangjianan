/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 16_Pass_by_reference.cpp
 * @brief {演示如何在C++中使用按引用(reference)传递.通过引用传递允许函数修改它在调用函数中提供的参数,这与按值传递相反.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 一个保持跑步计数的简单对象.
class Counter
{

// 单个私有(private)成员变量计数(count)以跟踪跑步计数.
private:
  int count;

public:

  // 将计数(count)初始化为 0.
  Counter()
  {
    count = 0;
  }
  
  // 输出计数(count)
  void print()
  {
    cout << "count: " << count << endl;
  }
  
  // 将计数(count)增加 1.
  void increment()
  {
    count++;
  }
  
};

// 使用按引用传递来修改调用函数中的计数器(Counter)对象,将其递增 3 次.
// 如果我们不使用 & 进行引用传递，调用函数中的对象将不会被修改.
// 而是将对象的"浅拷贝"("shallow copy")提供给函数(就像对象的副本,复制了成员变量值).
// 如果对象使用堆(heap)上的数据(即动态分配的变量)，则对象的浅拷贝可能是危险的(或至少容易出错).
void increment_3x(Counter &counter) 
{
  counter.increment();
  counter.increment();
  counter.increment();
}

// 使用通过引用将 x 递增 1,即在调用函数中传递给递增的任何变量.
void increment(int& x)
{
  x++;
  cout << "x after increment: " << x << endl;
}

// 通过按引用传递重要的是允许我们在调用环境中一次修改两个变量,
// 与按值传递不同,我们通常返回单个值,该值分配给调用函数中的单个变量.
void swap(int &x, int &y)
{
  int temp = x;
  x = y;
  y = temp;
}

int main()
{
  int a = 4;
  
  // 因为增量使用通过引用传递,'a' 将增加 1!
  cout << "a before: " << a << endl;
  increment(a);
  cout << "a after: " << a << endl;
  
  int b = 4;
  int c = 8;

  // 我们可以交换 b 和 c,在调用函数中一次有效地改变两个变量!
  cout << "BEFORE b: " << b << ", c: " << c << endl;
  swap(b,c);
  cout << "AFTER b: " << b << ", c: " << c << endl;
  
  
  Counter counter;
  
  // 通过引用传递也适用于对象,例如计数器(Counter)对象
  counter.print();
  increment_3x(counter);
  counter.print();
  
  return 0;
}