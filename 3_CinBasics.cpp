/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 3_CinBasics.cpp
 * @brief {演示如何使用cin通过C++接受用户的输入.}
 ********************************************************************************/

// cin 包含在 iostream 库中.
#include <iostream>

using namespace std;

int main()
{
  
  // 提示用户输入一个 int 类型的值.
  int x;
  cout << "Enter an integer: ";

  // 使用 cin 将用户输入的值存储到 x...,如果由于某种原因,
  // 它无法完成的话,则表达式将判断为 False.
  if (cin >> x)
  {
    // 如果成功接收到整数,则输出该值.
    cout << "You entered the integer " << x << endl;
  }
  else
  {
    // 如果有错误,清除错误,并通知用户
    // 他们提供了无效的输入.
    cin.clear();
    cout << "Invalid input" << endl;
  }
  
  // 将清除任何字符的输入流,最多 1000 个字符
  // 或第一个换行符,以先到者为准.
  cin.ignore(1000, '\n');
  

  // 矩形的维度、面积的变量
  double height = 0;
  double width = 0;
  double area = 0;
  
  // 提示用户输入高度和宽度值.
  cout << "Enter height and width (separated by a space):";

  // 我们可以使用 cin 一次接收多个值,因此输入一个由空字符分割的高度值
  // 和宽度值,将起到作用.
  // e.g. 输入 5  20.5
  cin >> height >> width;
  area = height * width;
  cout << height << " x " << width << " = " << area << endl;
  

  // 清除任何字符的输入流 (e.g. 换行符)
  cin.ignore(1000, '\n');
  
  // 提示用户输入他们的名字.
  string name;
  cout << "Enter your name: ";
  
  // 将字符串存储到第一个空格字符为止,因此像 
  // 'kevin browne' 这样的名字将仅作为 'kevin' 存储到 name,
  // cin >> name;
  
  // 将字符串存储到 name 中的第一个换行符为止,
  // 所以像 'kevin browne' 这样的名字将被完全存储.
  getline(cin, name);

  // 用他们的名字向用户示好 :-)
  cout << "Hello " << name << "!" << endl;
  
  return 0;
}