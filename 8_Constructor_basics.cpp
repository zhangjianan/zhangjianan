/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 8_Constructor_basics.cpp
 * @brief {演示在C++中使用构造函数的基础知识.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 表示猫(Cat)的对象类型
class Cat
{

// 猫会有名字、颜色和最喜欢的玩具
private:
  string name;
  string color;
  string favorite_toy;
  
public:
  
  // 成员函数:打印出猫的信息.
  void print_cat()
  {
    cout << "Name: " << name << endl;
    cout << "Color: " << color << endl;
    cout << "Favourite Toy: " << favorite_toy << endl;
  }
  
  // 没有参数的构造函数,我们将成员变量初始化为默认值.
  Cat()
  {
    name = "Unknown";
    color = "Unknown";
    favorite_toy = "Unknown";
  }
  
  // 带有单个参数的构造函数,我们用它来初始化猫名.
  Cat(string n)
  {
    name = n;
    color = "Unknown";
    favorite_toy = "Uknown";
  }
  
  // 构造函数的参数可以有默认值,例如默认参数 favorite toy 的值为 "Laser Pointer", 
  // 我们还可以在类之外定义构造函数,例如这个例子. 
  // 只要我们在类中保留函数声明(即原型).
  Cat(string n, string c, string ft = "Laser Pointer");
  
};

// 我们使用语法 ClassName::ClassName( ... ) 来定义类外部的构造函数...
Cat::Cat(string n, string c, string ft)
{
  name = n;
  color = c;
  favorite_toy = ft;
}


int main()
{
  // 创建对象时,将调用不带参数的构造函数.
  Cat cat1;
  
  cout << "Cat 1..." << endl;
  cat1.print_cat();
  cout << endl;
  
  
  // 创建对象时,将调用接受单个参数的构造函数.
  Cat cat2("Spot");
  
  cout << "Cat 2..." << endl;
  cat2.print_cat();
  cout << endl;
  

  // 创建对象时,将调用接受 3 个参数的构造函数,但默认值将用于第 3 个参数.
  Cat cat3("Garfield", "Orange");
  
  cout << "Cat 3..." << endl;
  cat3.print_cat();
  cout << endl;
  
  return 0;
}