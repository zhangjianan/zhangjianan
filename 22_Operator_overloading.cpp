/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 22_Operator_overloading.cpp
 * @brief {C++ 中的操作符重载示例.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 定义一个简单的 Number 类
class Number
{

public:
  int n;
  
  Number(int set_n)
  {
    n = set_n;
  }
  
  // 重载 + 操作符，函数头必须遵循特定格式，具体格式取决于操作符.
  // + 运算符的左操作数将成为调用此成员函数的"this"对象,+ 运算符的右操作数将作为函数的参数提供.
  // 该函数应返回一个 Number 对象. 
  Number operator+(const Number &numA)
  {
    // 创建并返回一个新的 Number 对象,它的 n 个成员变量初始化为两个操作数对象的 n 个成员变量之和.
    return Number(this->n + numA.n);

    // 我们*可以*提供一个没有意义的重载操作符的定义,例如:总是返回一个 n 成员变量值为 0 的 Number 对象.
    // 但是我们不应该这样做,因为这会使我们的程序更难理解,我们应该尝试提供对操作符的标准目的有意义的定义.
    //
    // return Number(0);
  }
  
  // 重载相等操作符 ==,它应该返回一个布尔值(考虑到相等操作符的作用,这是有道理的).
  // 同理,等式运算符的左操作数将是调用此成员函数的"this"对象,而等式运算符的右操作数将作为该函数的参数提供.
  bool operator==(const Number &numA)
  {
    // 如果两个对象的 n 个成员变量相等,则返回 true,否则返回 false.
    if (this->n == numA.n) return true;
    else return false;
  }
  
};

int main()
{
  // 创建 2 个 number 对象
  Number a(5);
  Number b(10);
  
  // 如果我们没有重载 + 运算符，这会导致错误,
  // 但是我们的成员函数运行,并且 c 设置为该函数的返回值('a' 成为 'this' 对象,而 'b' 成为参数对象)
  Number c = a + b;
  
  // 输出结果,我们会得到 c.n: 15
  cout << "c.n: " << c.n << endl;
  
  // 测试重载的相等运算符(两个对象不相等的情况).
  if (a == c) cout << "a == c" << endl;
  else cout << "a != c" << endl;
  
  // 测试重载的相等运算符(两个对象不相等的情况).
  Number d(15);
  if (c == d) cout << "c == d" << endl;
  else cout << "c != d" << endl;
  
  return 0;
}