/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 35_Setbase.cpp
 * @brief {在 C++ 中使用 setbase 将整数输出为十六进制和八进制数的示例.}
 ********************************************************************************/

#include <iostream>

// setbase 在 iomanip 库中,所以我们必须包含它才能使用它.
#include <iomanip>

using namespace std;

int main()
{
  int number = 17;
  
  // 通常整数将以基于 10 输出,即十进制,通常表示数字的方式.
  cout << "number: " << number << endl;
  
  // setbase 是一个流操纵器,如果我们使用它传入值 8,如下所示,流中的未来 int 输出将输出为 base 8(八进制)数字.
  cout << "number: " << setbase(8) << number << endl;

  // 如果我们传入值 16,则后续整数将使用基于 16 输出,即十六进制数
  cout << "number: " << setbase(16) << number << endl;

  // 我们可以使用 setbase(10) 将其切换回基于 10 / 十进制
  cout << "number: " << setbase(10) << number << endl;
  
  // 将数字 0-32 输出为十进制、八进制和十六进制数字.
  for (int i = 0; i <= 32; i++)
  {
    cout << setbase(10) << i << " ";
    cout << setbase(8) << i << " ";
    cout << setbase(16) << i << endl;
  }
  
  return 0;
}