/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 12_Class_variables.cpp
 * @brief {演示如何在C++中使用类变量(也称为静态(static)成员变量).}
 ********************************************************************************/

#include <iostream>

using namespace std;

class Animal
{
public:

  // 常规("regular")成员变量,也成为实例变量.
  string species;

  // *class* 成员变量,也称为静态(static)成员变量, 
  // 这种变量是附加("attached")到类的,并且 *class* 有一个共同的总变量.
  static int total;

  // 我们可以在类定义中初始化一个 const 类变量.
  const string planet = "Earth";
  
  // 我们递增类变量,就像它是构造函数中这样的成员变量,
  // 用来识别另外一个 Animal 对象被创建. 
  Animal(string animal_species)
  {
    species = animal_species;
    total++;
  }
  
  // 当我们使用析构函数删除 Animal 对象时,我们 total 值 -1.
  ~Animal()
  {
    total--;
  }
  
};

// 我们使用下面的 ClassName::variable 语法在类定义之外初始化一个非常量(non-const)的类变量.
int Animal::total = 0;

int main()
{
  // 当我们创建 Animal 对象时,构造函数将运行,类变量 total 将增 1.
  Animal *lion = new Animal("lion");
  Animal *tiger = new Animal("tiger");
  
  // 实例成员变量与每个对象实例相关联,并且可能因每个对象而有差异.
  cout << "Lion: " << lion->species << endl;
  cout << "Tiger: " << tiger->species << endl;

  // 类成员变量与类绑定...我们可以使用 ClassName::variable 语法访问它.
  cout << "Total: " << Animal::total << endl;
  
  // 每次我们制作一个新的 Animal 对象时，total 都会增加.
  Animal *bear = new Animal("bear");
  
  // 请注意，我们可以使用如下对象实例访问类变量,
  // 但"最佳方式"是像我们上面那样使用类名来访问它.
  cout << "Total: " << tiger->total << endl;
  
  // 当我们删除一个对象时，会调用析构函数，从而减少 total!
  delete lion;
  
  // 现在 total 只有 2.
  cout << "Total: " << Animal::total << endl;
  
  // 删除剩余的对象.
  delete tiger;
  delete bear;

  // 现在 total 是 0 (请注意:即使不存在该类型的对象,我们也可以访问类变量).
  cout << "Total: " << Animal::total << endl;

  return 0;
}