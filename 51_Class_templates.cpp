/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-27
 * 
 * @file 51_Class_templates.cpp
 * @brief {在 C++ 中使用类模板的示例.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 我们希望有一个类来表示数组和支持运算,
// 但我们希望避免为不同类型(如 int、string 等)和不同的数组大小定义单独的类.
// 这会导致大量的代码重复.所以我们使用类模板(class template).
//
// 我们使用带有 2 个参数的模板(template)前缀来定义类模板'class template'.  
// T 是一个模板参数，我们将通过提供模板参数来使用它来创建类 Array 的特定类型版本.
// 我们还将提供一个length int 参数来定义数组(array)的长度(length).
// 每当我们尝试使用类模板创建对象实例时,都会提供这些参数.
//
// 当我们定义一个类模板,然后通过创建对象实例来使用它时,
// 编译器在编译时将通过将模板形参(parameters)替换为提供的模板实参(arguments)来创建类.
// 所以类模板定义了一系列潜在的类,我们如何使用类模板决定了编译器将创建哪些特定类以供在运行时使用.
// 
template <typename T, int length>
class Array
{
public:
  // 在内部,我们的 Array 类使用一个数组来存储数组数据.
  // 公共(public)成员变量数组的类型和长度将由提供的模板参数确定.
  T array[length];
  
  // 用由 T 确定的类型的值来 fill(填充) 数组.
  void fill(T value)
  {
    for (int i = 0; i < length; i++)
      array[i] = value;
  }
  
  // 返回对给定索引处类型 T 的数组元素的引用.
  T& at(int index)
  {
    return array[index];
  }
};


int main()
{
  // 因为我们使用 Array<int, 5>,编译器会根据上面的类模板为我们创建一个类...
  // 将 T 替换为 int,并将长度(length)替换为 5!
  // 然后在 *runtime* 执行程序将使用编译器创建的此类创建一个对象实例.
  //
  Array<int, 5> intArr;
  
  // 用值 2 填充数组.
  intArr.fill(2);
  
  // 如果我们在索引 2 处输出数组元素的值,我们将得到 4.
  cout << "intArray[4]: " << intArr.at(4) << endl;
  cout << "-----------" << intArr.at(4) << endl;

  // 我们再次使用类模板,并且在 copmile-time 编译器将通过在我们的类模板数组中用字符串(string)替换 T 和用 8 替换长度(length)来创建一个类. 
  // 然后在运行时执行程序将使用这个类创建一个对象实例.
  Array<string, 8> strArr;
  
  // 用字符串 abc 填充数组.
  strArr.fill("abc");

  // 但是将索引 6 处的数组元素(倒数第二个)设置为“123”.
  strArr.at(6) = "123";
  
  // 输出数组元素值,除倒数第二个数组元素外,除倒数第二个元素应为“123”外,其余数组元素应为“abc”.
  for (int i = 0; i < 8; i++)
    cout << "strArr[" << i << "]: " << strArr.at(i) << endl;
  
  // 请注意,因为编译器使用我们的类模板在编译时创建特定的类,
  // 我们不能将类模板与不是长度(lenght)参数的常量(const)表达式一起使用.
  // 在这里,我们尝试使用在运行时通过我们的类模板设置的变量 x...
  // 但是编译器不知道'x'会是什么,所以它只是不能使用类模板来创建一个类.
  // 如果我们取消注释下面的行,我们将看到结果错误.
  //
  // int x;
  // cin >> x;
  // Array<double, x> xArr;
  
  return 0;
}