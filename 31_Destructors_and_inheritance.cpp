/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 31_Destructors_and_inheritance.cpp
 * @brief {当我们有一个基类和一个派生类时,析构函数如何在 C++ 中使用继承的示例.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 一个简单的基类.
class BaseClass
{
public:
  
  // 基类构造函数.
  BaseClass()
  {
    cout << "BaseClass 构造函数正在执行..." << endl;
  }
 
  // 基类析构函数.
  ~BaseClass()
  {
    cout << "BaseClass 析构函数正在执行..." << endl;
  }
  
};

// 从上述基类继承的简单派生类.
class DerivedClass : public BaseClass
{
public:
  
  // 派生类构造函数.
  DerivedClass()
  {
    cout << "DerivedClass 构造函数正在执行..." << endl;
  }
  
  // 派生类析构函数.
  ~DerivedClass()
  {
    cout << "DerivedClass 析构函数正在执行..." << endl;
  }
};

void func()
{
  // 当我们声明示例时,派生类将在此时构建.
  //
  // 对于派生类中的构造函数,首先运行基类构造函数,然后运行派生类构造函数.
  //
  DerivedClass example;
  
  // output 和 endline 分隔构造函数/析构函数输出.
  cout << endl;

  // 当函数返回时,示例对象将被销毁.
  //
  // 对于析构函数,首先运行派生类析构函数,然后运行基类析构函数...
  // 构造函数的相反顺序!
}

int main()
{
  // 调用函数 func 以便我们可以创建和销毁 DerivedClass 对象实例以探索析构函数的工作原理.
  func();
  
  // 输出结束线以将上述函数的任何输出与程序输出的任何结尾分开.
  cout << endl;
  
  return 0;
}