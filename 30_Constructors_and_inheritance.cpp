/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 30_Constructors_and_inheritance.cpp
 * @brief {演示构造函数如何在 C++ 中使用继承.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// MenuItem 是具有单个公共成员变量的简单基类
class MenuItem
{
public:
  string name;
  
  // 尝试取消注释这个默认构造函数,然后对下面建议的派生类默认构造函数进行更改,看看会发生什么!
  /*
  MenuItem()
  {
    name = "unknown";
    cout << "Base Class Default Constructor" << endl;
  }
  */
  
  // MenuItem 的参数化构造函数.
  MenuItem(string set_name)
  {
    name = set_name;
    cout << "基类-带参数构造函数" << endl;
  }
};

// Drink 是一个简单的派生类,只有一个公共成员变量.
class Drink : public MenuItem
{
public:
  double ounces;
  
  // Drink 用这条语句继承了 MenuItem 的所有构造函数!
  using MenuItem::MenuItem;

  // 在上面的 MenuItem 中取消注释默认构造函数后,将下面的 Drink 默认构造函数头替换为这个:Drink()
  //
  // 我们会发现调用默认的 Drink 构造函数会首先调用默认的 MenuItem 构造函数!

  // Drink 的默认构造函数使用参数 "unknown" 调用 MenuItem 的参数化构造函数.
  Drink() : MenuItem("unknown")
  {
    ounces = 8;
    cout << "派生类-默认构造函数" << endl;
  }
  
  // Drink 的参数化构造函数使用参数 "n/a" 调用 MenuItem 的参数化构造函数.
  Drink(double set_ounces) : MenuItem("n/a")
  {
    ounces = set_ounces;
    cout << "派生类-带参数构造函数" << endl;
  }
  
};

int main()
{
  // 将调用参数化的 MenuItem(基类)构造函数,然后调用参数化的 Drink(派生类)构造函数.
  Drink hot_chocolate1("hot_chocolate");
  
  // 将调用参数化的 MenuItem 构造函数,然后调用默认的 Drink 构造函数...
  // 除非我们进行上述更改,在这种情况下,将调用 MenuItem 的默认构造函数,然后调用 Drink 的默认构造函数.
  //
  // 取消注释以尝试...
  //
  cout << "------" << endl;
  Drink hot_chocolate2;

  // 将调用继承的参数化 MenuItem 构造函数...
  // 并且不会调用为 Drink 对象显式定义的构造函数,
  // 因为我们现在也使用继承"使" MenuItem 的参数化构造函数成为 Drink 对象的构造函数!
  //
  // 取消注释以尝试...
  //
  cout << "------" << endl;
  Drink hot_chocolate3(20);

  return 0;
}