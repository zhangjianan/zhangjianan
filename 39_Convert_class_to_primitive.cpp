/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 39_Convert_class_to_primitive.cpp
 * @brief {演示如何在 C++ 中将用户定义的类类型转换为基本类型(如 double、char 等).}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 创建一个简单的 Grade 类
class Grade
{

// Grade 对象将有一个成员变量等级,旨在设置为 92.45、45.35 等值.
private:
  double grade;
  
public:
  
  // 用于初始化 grade 成员变量的构造函数.
  Grade(double grade) : grade(grade) {}
  
  // operator type() 允许我们创建一个返回指定类型值的函数,
  // 定义如何将对象实例转换为类型.
  //
  // 在这种情况下,我们通过简单地返回等级成员变量的值来定义如​​何将等级对象转换为双精度值...
  operator double()
  {
    return grade;
  }
  
  // 在将 Grade 对象转换为 char 的过程中,我们根据等级值的范围返回“字母等级”A、B、C、D 或 F.
  operator char()
  {
    if (grade >= 80) return 'A';
    else if (grade >= 70) return 'B';
    else if (grade >= 60) return 'C';
    else if (grade >= 50) return 'D';
    else return 'F';
  }
};

int main()
{
  // 实例化一个 Grade 对象.
  Grade grade1(93.22);
  
  // 将 Grade 对象实例分配给 double 变量将导致发生类型转换,正如我们使用 operator 指定的那样,产生值 93.22.
  double dbl_grade1 = grade1;
  cout << "dbl_grade1: " << dbl_grade1 << endl;
  
  // 将 Grade 对象实例分配给 char 变量将导致发生我们使用运算符指定的类型转换,从而产生值“A”.
  char chr_grade1 = grade1;
  cout << "chr_grade1: " << chr_grade1 << endl;
  
  return 0;
}