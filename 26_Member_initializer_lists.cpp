/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 26_Member_initializer_lists.cpp
 * @brief {在 C++ 中如何、为什么以及何时使用成员初始化器列表的示例.}
 ********************************************************************************/

#include <iostream>
using namespace std;

// 具有默认构造函数和参数化构造函数的类.
class Major
{
public:
  string name;
  
  // 我们将成员变量名称初始化为值 "Undeclared".
  Major() : name("Undeclared")
  {
    // 我们仍然可以将代码放入将在成员初始化后运行的 instructor 中,以执行构造函数可能需要执行的其他工作.
    cout << "Major Constructor (Undeclared)" << endl;
  } 
  
  // 初始化成员变量 name 到参数 name 的值.
  Major(string name) : name(name)
  {
    cout << "Major Constructor (" << name << ")" << endl;
  }
};

// 没有默认构造函数的类.
class Minor
{
public:
  string name;
  
  // 初始化成员变量 name 到参数 name 的值.
  Minor(string name) : name(name) {}
};

class Student
{
public:
  string name;
  int start_year;
  int grad_year;

  // 我们可以像这样初始化一个 const 成员变量,但它会为每个对象实例采用相同的值.
  // const string id = "abc";
  
  // 如果我们希望为对象的 const 成员变量提供可能不同的值,我们需要使用成员初始化器列表,如下所示!
  const string id;

  // 如果一个类有一个默认构造函数和一个参数化构造函数,
  // 然后将调用默认构造函数和参数化构造函数来创建对象 *if* 除了下面的成员变量 major 声明之外,
  // 我们还在构造函数主体中使用类似的东西对其进行了初始化:
  //
  // Major major("something");
  //
  // 只有当我们在成员初始化器列表中初始化对象时,我们才能防止这种情况发生并且只创建一次对象.
  // 出于性能原因,这很重要,否则我们会在只需要一个构造函数时调用两个构造函数!
  Major major;

  // 我们必须使用成员初始化器列表来初始化引用成员变量,例如 &minor1
  Minor &minor1;

  // 如果一个对象成员变量只有一个参数化的构造函数,
  // 那么我们还必须使用成员初始化器列表来初始化成员变量.
  Minor minor2;
  
  Student(string name, long int start_year, string major,Minor &minor) :
    // 除了 ( ) 之外，我们还可以在成员初始化器列表中使用 { },
    // 不同之处在于 { } 将检测类型缩小错误...
    // 例如:如果我们取消注释 start_year 初始化的 { } 版本并注释掉 start_year 初始化的 ( ) 版本,
    // 我们将得到一个错误,因为 int(start_year 成员变量的类型)是一种比 long int(start_year 参数的类型)更窄的类型,能够表示更少的数字.
    name{name},
//    start_year{start_year},
    start_year(start_year),
    grad_year(start_year + 4),
    id(name.append(to_string(start_year))),
    major(Major(major)),
    minor1(minor),
    minor2(Minor("Physics"))
  {
    cout << "Student Object Constructed!" << endl;
  }
};

// 当派生类有没有默认构造函数的基类时,我们需要使用成员初始化器列表来使用基类构造函数.
class MatureStudent : Student
{
public:
  int age;
  
  // 我们使用成员初始化列表来使用 Student 的构造函数...
  MatureStudent(int age, string name,
                long int start_year, string major,
                Minor &minor) :
   Student(name, start_year, major, minor),
   age(age) {}
};

int main()
{
  // 测试创建一个 Student 对象,因为现在编写了代码,这个 long int 值不应该导致编译器错误.
  // (尽管由于类型缩小问题,start_year 和 grad_year 的 int 值将不正确).  
  //
  // 其他一切都应该按预期工作!
  //
  long long int year = 999999999999;
  Minor minor("Economics");
  Student s1("John", year, "Computer Science", minor);
  
  cout << "Name: " << s1.name << endl
       << "Start Year: " << s1.start_year << endl
       << "Grad Year: " << s1.grad_year << endl
       << "ID: " << s1.id << endl
       << "Major: " << s1.major.name << endl
       << "Minor1: " << s1.minor1.name << endl
       << "Minor2: " << s1.minor2.name << endl;

  return 0;
}