#include <iostream>
/***********************************
 * @brief {class of SH}
 **********************************/
class ShangHai
{  
public:
  std::string word;
  void print()
  {
    std::cout << word << std::endl; 
  }
  ShangHai(std::string S):word(S){std::cout << "Start.20220331..." << std::endl;}
  ~ShangHai(){std::cout << "End.20220601!" << std::endl;}
};
/**********************************
 * @brief {Process of SH}
 * @return int 
 *********************************/
int main()
{
  ShangHai *sh = new ShangHai("Closing");
  (*sh).print();

  delete sh;
  return -1;
}

