/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 14_Array_of_objects.cpp
 * @brief {在 C++ 中声明和初始化对象数组的示例程序.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// 一个简单的方形(Square)类
class Square
{
public:

  // 方形(squares)将拥有长度(length)与颜色(color).
  int side_length;
  string color;
  
  // 成员函数:打印方形(square)数据.
  void print()
  {
    cout << "side length: " << side_length;
    cout << ", color: " << color << endl;
  }
  
  // 初始化 Square 对象的默认构造函数,不带参数.
  Square()
  {
    side_length = 0;
    color = "black";
  }
  
  // 使用 2 个参数初始化 Square 对象的构造函数.
  Square(int set_length, string set_color)
  {
    side_length = set_length;
    color = set_color;
  }
  
  // 使用 1 个参数初始化 Square 对象的构造函数.
  Square(int set_length)
  {
    side_length = set_length;
    color = "black";
  }

};

int main()
{
  // 这将使用默认构造函数初始化数组的 Square 元素...
  // 如果我们没有定义默认构造函数(没有参数的构造函数),
  // 那么 C++ 将使用一个"隐式"("implicit")默认构造函数,以默认方式初始化对象的成员变量.
  // 如果我们定义了一个接受 1 个或多个参数的构造函数并且我们没有定义默认构造函数,那么这实际上会导致错误！
  //
  // Square squares[3];

  // 这将使用带有 1 个参数的构造函数为每个数组元素初始化 3 个 Square 对象.
  // 如下这样初始化数组:
  // Square squares[3] = {Square(2), Square(4), Square(8)};
  //
  // Square squares[3] = {2,4,8};

  // 我们可以使用带有 2 个参数的构造函数来初始化数组元素,如下所示:
  //
  // Square squares[3] = {Square(2,"orange"), Square(4, "red"), Square(8,"green")};
  
  // 如果我们在初始化数组时提供少于所需数量的值或构造函数调用,
  // 将调用默认构造函数来初始化剩余的数组元素.   
  // 例如，下面的代码将使用带有 1 个参数的构造函数来初始化第 1 个数组元素
  // 并使用默认构造函数来初始化剩余的 2 个元素:
  // Square squares[3] = {4};

  // 将使用单参数构造函数初始化第 1 个元素,
  // 第 2 个元素使用双参数构造函数，第 3 个元素使用默认构造函数.
  Square squares[3] = {2,Square(5,"red")};
  
  // 打印出每个 square.
  for (int i = 0; i < 3; i++)
    squares[i].print();
  
  return 0;
}