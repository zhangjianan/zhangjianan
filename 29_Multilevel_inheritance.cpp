/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 29_Multilevel_inheritance.cpp
 * @brief {C++ 中的多级继承演示.}
 ********************************************************************************/

#include <iostream>

using namespace std;

// MenuItem 是 Drink 类的基类(即"父类",这使其成为 HotDrink 类的"祖父").
class MenuItem
{
public:

  // 菜单项将有 name 和 calories 数.
  string name;
  double calories;

  // 打印菜单项成员变量的成员函数.
  void print()
  {
    cout << name << " (" << calories << " cal)" << endl;
  }
};

// Drink 是 MenuItem 的派生类和 HotDrink 的基类.
// 作为 MenuItem 的派生类,它将继承成员变量 name 和 calories 以及成员函数 print.
class Drink : public MenuItem
{
public:

  // Drink 对象将有一个额外的成员变量,即 drink 中的盎司数(ounces).
  double ounces;
};

// HotDrink 是 Drink 的派生类,使 Drink 类成为"父类",MenuItem 类成为"祖父类".
// 作为 Drink 的派生类,它将继承成员变量 ounces 以及成员变量 name 和 calories,
// 以及从 MenuItem 继承的 Drink 的成员函数 print!
class HotDrink : public Drink
{
public:

  // 饮料(drink) 也有饮用温度
  double temperature;
  
  // 输出热饮上菜指令的成员函数.
  void serving_instructions()
  {
    cout << "Serve " << ounces << " ounces at " << temperature << " degrees F" << endl;
  }
};

int main()
{
  // 创建一个 HotDrink 对象实例.
  HotDrink hot_chocolate;
  
  // 注意:我们如何访问所有的成员变量,包括那些通过多级继承继承的变量.
  hot_chocolate.name = "Hot Chocolate";
  hot_chocolate.calories = 200;
  hot_chocolate.ounces = 8;
  hot_chocolate.temperature = 77;
  
  // 注意:我们如何调用通过多级继承继承的 print 成员函数
  hot_chocolate.print();
  hot_chocolate.serving_instructions();
  
  return 0;
}

//  多级继承的可视化
//
//
//        基类 X
//           ↑
//           |
//           | - Y 继承自 X
//           |
//           |
//      派生 & 基类 Y
//           ↑
//           |
//           | - Z 继承自 Y
//           |
//           |
//        派生类 Z
//
//
//  Y 是 Z 的 "父类",我们可以将 X 视为 Z 的 "祖父类".