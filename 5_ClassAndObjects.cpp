/*********************************************************************************
 * @copyright Copyright (c) 2022  视觉算法与编程实验室
 * 
 * @author ZhangJianAn (zhangja@qq.com)
 * @version 0.1
 * @date 2022-05-26
 * 
 * @file 5_ClassAndObjects.cpp
 * @brief {一个基本的示例,旨在介绍C++中的类、对象以及面向对象编程.}
 ********************************************************************************/

#include <iostream>
#include <string>

using namespace std;

// 创建一种新类型的对象,用来表示银行账户.
class BankAccount
{

// 公共(public) 访问说明符确保以下的成员变量、函数在类外可用.
public:
  
  // 成员变量:账户持有人姓名(name)、账户余额(balance).
  string name;
  int balance;
  
  // 成员函数:从账户中提取金额.
  void withdraw(int amount)
  {
    // 可用从成员函数中访问、修改成员变量
    balance = balance - amount;
  }
  
  // 成员函数:打印账户信息.
  void print()
  {
    cout << name << " has " << balance << " dollars " << endl;
  }
};

int main()
{
  // 创建一个 BankAccount 实例/对象,并设置成员变量.
  BankAccount account1;
  account1.name = "张三";
  account1.balance = 3000;
  
  // 调用成员函数.
  account1.print();
  
  // 创建另外一个不同的 BankAccount 对象... 
  // 我们可以创建任意数量的类实例!  
  BankAccount account2;
  account2.name = "李四";
  account2.balance = 1000;
  
  // 打印出提取金额(withdraw())前后的账户信息. 
  account2.print();
  account2.withdraw(100);
  account2.print();

  return 0;
}